import 'package:flutter/material.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:flutter_app_lang/views/dictionary.dart';
import 'package:flutter_app_lang/views/views.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'styles/theme_colors.dart' as app_colors;
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize Firebase.
  await Firebase.initializeApp();
  await Preferences.init();
  runApp(const AppState());
}

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => AuthService())],
      child: const MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: 'check_auth',
      routes: {
        'check_auth': (BuildContext context) => const CheckAuth(),
        'welcome': (BuildContext context) => const WelcomeView(),
        'login': (BuildContext context) => const LoginView(),
        'signup1': (BuildContext context) => const RegisterView1(),
        'signup2': (BuildContext context) => const RegisterView2(),
        'home': (BuildContext context) => const Home(),
        'dictionary_add': (BuildContext context) => const DictionaryAddView(),
        'dictionary_IR_feedback': (BuildContext context) =>
            const DictionaryIRFeedbackView(),
        'dictionary': (BuildContext context) => const DictionaryView(),
        'onboardingNL': (BuildContext context) => const OnboardingNL(),
        'onboardingLL': (BuildContext context) => const OnboardingLL(),
        'landmarks': (BuildContext context) => const LandmarksView(),
        'landmark_add': (BuildContext context) => const LandmarkAdd(),
        'changeLang': (BuildContext context) => const ChangeLL(),
      },
      scaffoldMessengerKey: NotificationsService.messengerKey,
      //Material 2 theme
      theme: ThemeData(
          colorScheme: ColorScheme(
            brightness: Brightness.light,
            primary: app_colors.primary,
            onPrimary: app_colors.onPrimary,
            primaryVariant: app_colors.primaryContainer,
            secondary: app_colors.secondaryContainer,
            onSecondary: app_colors.onSecondaryContainer,
            secondaryVariant: app_colors.tertiary, //To be used as Tertiary
            error: app_colors.errorContainer,
            onError: app_colors.onErrorContainer,
            background: app_colors.background,
            onBackground: app_colors.onBackground,
            surface: app_colors.surface,
            onSurface: app_colors.onSurface,
          ),
          textTheme: TextTheme(
            headline1: GoogleFonts.oxygen(
                fontSize: 57,
                fontWeight: FontWeight.w700,
                letterSpacing: -1.0,
                color: Colors.black),
            headline2: GoogleFonts.oxygen(
                fontSize: 45,
                fontWeight: FontWeight.w700,
                letterSpacing: -0.5,
                color: Colors.black),
            headline3: GoogleFonts.oxygen(
                fontSize: 36, fontWeight: FontWeight.w700, color: Colors.black),
            headline4: GoogleFonts.oxygen(
                fontSize: 32, fontWeight: FontWeight.w400, letterSpacing: 0.25),
            headline5:
                GoogleFonts.oxygen(fontSize: 28, fontWeight: FontWeight.w400),
            headline6: GoogleFonts.oxygen(
                fontSize: 24, fontWeight: FontWeight.w400, letterSpacing: 0.15),
            subtitle1: GoogleFonts.oxygen(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                letterSpacing: -0.15),
            subtitle2: GoogleFonts.oxygen(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                letterSpacing: -0.15),
            bodyText1: GoogleFonts.roboto(
                fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
            bodyText2: GoogleFonts.roboto(
                fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
            button: GoogleFonts.roboto(
                fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
            caption: GoogleFonts.roboto(
                fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
            overline: GoogleFonts.roboto(
                fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
          )),
      //change to const MyHomePage(title: 'Select current language') in order to test fonts
    );
  }
}
