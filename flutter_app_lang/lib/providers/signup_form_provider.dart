import 'package:flutter/material.dart';

class SignUpProvider extends ChangeNotifier {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String name = '';
  String lastname = '';
  String country = '';
  String birthDate = '';
  String email    = '';
  String password = '';

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  
  set isLoading( bool value ) {
    _isLoading = value;
    notifyListeners();
  }
  
  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }

}