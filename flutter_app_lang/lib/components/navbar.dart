import 'package:flutter/material.dart';
import '../styles/theme_colors.dart' as app_colors;

class Navbar extends StatefulWidget {
  const Navbar({Key? key}) : super(key: key);

  @override
  State<Navbar> createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  static int _selectedIndex = 0;

  void _onItemTapped(int index) {
    if (index != _selectedIndex) {
      if (index != 2) {
        //Toggle comment when more features are implemented
        setState(() {
          _selectedIndex = index;
        });
        switch (_selectedIndex) {
          case 0:
            Navigator.pushReplacementNamed(context, 'home');
            break;
          case 1:
            Navigator.pushReplacementNamed(context, 'dictionary');
            break;
          // Toggle comment when implemented
          // case 2:
          //   Navigator.pushReplacementNamed(context, 'cards');
          //   break;
          case 3:
            Navigator.pushReplacementNamed(context, 'landmarks');
            break;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home_rounded),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.collections_bookmark_outlined),
          activeIcon: Icon(Icons.collections_bookmark_rounded),
          label: 'Dictionary',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.library_books_outlined),
          activeIcon: Icon(Icons.library_books_rounded),
          label: 'Cards',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.location_on_outlined),
          activeIcon: Icon(Icons.location_on),
          label: 'Landmarks',
        ),
      ],
      currentIndex: _selectedIndex,
      backgroundColor: const Color(0xFFE7F1F4),
      selectedItemColor: app_colors.primary,
      unselectedItemColor: app_colors.outline,
      onTap: _onItemTapped,
    );
  }
}
