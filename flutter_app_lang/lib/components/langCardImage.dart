import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'clickableIcon.dart';

class LangCardImage extends StatefulWidget {
  const LangCardImage({
    Key? key,
    required this.color,
    required this.highlightColor,
    required this.image,
    required this.title,
    required this.subtitle,
    required this.onTap,
    this.actions,
  }) : super(key: key);

  final Color color;
  final Color highlightColor;
  final String title;
  final String subtitle;
  final String image;
  final List<ClickableIcon>? actions;
  final Function onTap;

  @override
  State<StatefulWidget> createState() => _LangCard();
}

class _LangCard extends State<LangCardImage> {
  Color colorCard = app_colors.surface;
  bool selected = false;
  List<Widget> actions = [];
  List<Widget> content = [];
  bool tappable = true;

  @override
  Widget build(BuildContext context) {
    colorCard = widget.color;
    ImageProvider<Object> image = const AssetImage("assets/no_image.jpg");
    if (widget.actions != null) {
      actions = widget.actions!;
      tappable = false;
    }
    if (widget.image != ""){
      image = NetworkImage(widget.image);
    }
    content = [
      ClipRRect(
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4), bottomLeft: Radius.circular(4)),
          child: Image(
            image: image,
            height: 80,
            width: 80,
            fit: BoxFit.cover,
          )),
      Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(widget.title, style: Theme.of(context).textTheme.bodyText1),
              Text(
                widget.subtitle,
                style: GoogleFonts.roboto(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.25,
                    color: app_colors.outline),
              ),
            ],
          )),
      const Spacer()
    ];
    content.addAll(actions);

    return Container(
        constraints: const BoxConstraints(minHeight: 80),
        padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
        child: Card(
            color: selected ? widget.highlightColor : widget.color,
            child: InkWell(
              splashColor: widget.highlightColor,
              onTap: () {
                if (tappable) {
                  setState(() {
                    selected = !selected;
                  });
                  widget.onTap();
                }
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: content,
              ),
            )));
  }
}