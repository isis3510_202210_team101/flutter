import 'package:flutter/material.dart';

class ClickableIcon extends StatefulWidget {
  const ClickableIcon({
    Key? key,
    required this.iconOn,
    this.iconOff,
    required this.type,
    required this.onTap,
  }) : super(key: key);

  final Icon iconOn;
  final Icon? iconOff;
  final TypeOfClickableButton type;
  final Function onTap;

  @override
  State<StatefulWidget> createState() => _LangCard();
}

class _LangCard extends State<ClickableIcon> {

  bool selected = false;
  EdgeInsets padding = const EdgeInsets.symmetric(horizontal: 0);

  @override
  Widget build(BuildContext context) {
    switch(widget.type) {
      case TypeOfClickableButton.edge:
        padding = const EdgeInsets.symmetric(horizontal: 16);
        break;
      case TypeOfClickableButton.unique:
        padding = const EdgeInsets.symmetric(horizontal: 0);
        break;
      case TypeOfClickableButton.middle:
        padding = const EdgeInsets.symmetric(horizontal: 8);
        break;
    }
    return (GestureDetector(
      child: Padding(
        padding: padding,
        child: selected? widget.iconOn : widget.iconOff,
      ),
      onTap: () {
          setState(() {
            selected = !selected;
          });
          widget.onTap();
      },
    ));
  }
}

enum TypeOfClickableButton{
  edge,
  unique,
  middle
}