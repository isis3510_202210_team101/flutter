import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StreakCard extends StatelessWidget {
  const StreakCard({
    Key? key,
    required this.color,
    required this.mainText,
    required this.subText,
    required this.trailIcon,
  }) : super(key: key);

  final Color color;
  final String mainText;
  final String subText;
  final Widget trailIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.topRight,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: color, width: 5)),
        child: Row(
          children: [
            Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(mainText,
                      style: Theme.of(context).textTheme.subtitle1),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    subText,
                    style: GoogleFonts.oxygen(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        letterSpacing: -0.15),
                  ),
                ),
              ],
            ),
            const Spacer(),
            trailIcon
          ],
        ),
        padding: const EdgeInsets.all(24),
      ),
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
    );
    ;
  }
}
