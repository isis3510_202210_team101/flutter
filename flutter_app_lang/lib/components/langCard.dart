import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../styles/theme_colors.dart' as app_colors;

class LangCard extends StatefulWidget {
  const LangCard({
    Key? key,
    required this.color,
    required this.highlightColor,
    this.leadingIcon,
    this.trailIcon,
    this.bottom,
    required this.title,
    this.subtitle,
    this.onTap,
  }) : super(key: key);

  final Color color;
  final Color highlightColor;
  final String title;
  final String? subtitle;
  final Widget? leadingIcon;
  final Widget? trailIcon;
  final Widget? bottom;
  final Function? onTap;

  @override
  State<StatefulWidget> createState() => _LangCard();
}

class _LangCard extends State<LangCard> {
  Color colorCard = app_colors.surface;
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    colorCard = widget.color;

    return Container(
      constraints: const BoxConstraints(minHeight: 80),
      padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
      child: Card(
          color: selected ? widget.highlightColor : widget.color,
          child: InkWell(
            splashColor: widget.highlightColor,
            onTap: widget.onTap == null
                ? () {}
                : () {
                    setState(() {
                      selected = !selected;
                    });
                    widget.onTap!();
                  },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                    leading: widget.leadingIcon,
                    trailing: widget.trailIcon,
                    title: Text(widget.title,
                        style: Theme.of(context).textTheme.bodyText1),
                    subtitle: widget.subtitle == null
                        ? null
                        : Text(widget.subtitle!)),
                widget.bottom == null ? Container() : widget.bottom!
              ],
            ),
          )),
    );
  }
}