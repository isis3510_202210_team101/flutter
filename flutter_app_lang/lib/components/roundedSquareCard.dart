import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RSCard extends StatelessWidget {
  const RSCard({
    Key? key,
    required this.color,
    required this.child,
    required this.title,
    required this.mainText,
    required this.subText,
  }) : super(key: key);

  final Color color;
  final String title;
  final String mainText;
  final String subText;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.topRight,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            color: color),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Text(title, style: Theme.of(context).textTheme.headline5),
            ),
            const Spacer(),
            Align(
              alignment: Alignment.centerLeft,
              child:
                  Text(mainText, style: Theme.of(context).textTheme.subtitle1),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                subText,
                style: GoogleFonts.oxygen(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                    letterSpacing: -0.15),
              ),
            ),
            const Spacer(),
            child
          ],
        ),
        padding: const EdgeInsets.all(24),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width - 70,
      padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
    );
  }
}