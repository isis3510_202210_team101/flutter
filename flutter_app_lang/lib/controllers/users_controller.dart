import 'dart:convert';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:flutter_app_lang/models/models.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:http/http.dart' as http;

class UsersController {
  static User? user;
  static List<Word> dictionary = [];
  static Map<String, bool> mapDetail = {};
  static String nextPageToken = "";

  static void resetDictionary() {
    dictionary = [];
  }

  static void resetDetail() {
    dynamic word;
    for (int i = 0; i < dictionary.length; i++) {
      word = dictionary[i];
      mapDetail[word.originalWord] = false;
    }
  }

  static void retrieveUser(login, setLearnedLanguages) {
    if (user == null) {
      getUser(login).then((respUser) {
        user = respUser;
        setLearnedLanguages([...user!.learnedLanguages, user!.nativeLanguage]);
      });
    } else {
      setLearnedLanguages([...user!.learnedLanguages, user!.nativeLanguage]);
    }
  }

  static Future setUserPreferences(login) async {
    User? respUser = await getUser(login);
    user = respUser;
    Preferences.nativeLang = user!.nativeLanguage;
    Preferences.currentLang = user!.learnedLanguages[0];
    Language? firstLearnedLanguage =
        await LanguagesController.getLanguage(user!.learnedLanguages[0]);
    Preferences.flag = firstLearnedLanguage.flag;
  }

  static void retrieveDictionaryList(
      login, nativeLang, mainLearningLang, setWordsDictionary) {
    if (dictionary.isEmpty) {
      getDictionary(login).then((respDictionary) {
        dictionary = respDictionary!;
        resetDetail();
        TranslationController.retrieveTranslations(
                dictionary, nativeLang, mainLearningLang)
            .then((value) {
          dynamic word;
          for (int i = 0; i < dictionary.length; i++) {
            word = dictionary[i];
            mapDetail[word.originalWord] = false;
          }
          resetDetail();
          setWordsDictionary();
        });
      }).catchError((a) {
        dictionary = [];
        resetDetail();
      });
    } else {
      setWordsDictionary();
    }
  }

  static Future appendDictionaryList(
      login, nativeLang, mainLearningLang, setWordsDictionary) async {
    getDictionary(login).then((respDictionary) {
      dictionary.addAll(respDictionary!);
      resetDetail();
      TranslationController.retrieveTranslations(
              dictionary, nativeLang, mainLearningLang)
          .then((value) {
        dynamic word;
        for (int i = 0; i < dictionary.length; i++) {
          word = dictionary[i];
          mapDetail[word.originalWord] = false;
        }
        setWordsDictionary();
      });
    }).catchError((a) {
      dictionary = [];
      resetDetail();
    });
  }

  static void getDetail(
      login, wordObj, mainLearningLang, setWordsDictionary) async {
    final resp = await http.get(Uri.parse(usersUrl +
        '/' +
        login +
        "/dictionaryPhotos/" +
        login +
        "-" +
        wordObj.idWord));
    String photo = json.decode(resp.body)["fields"]["photo"]["stringValue"];
    dictionary[dictionary.indexOf(wordObj)].photo = photo;
    TranslationController.getDescription(
        wordObj, mainLearningLang, setWordsDictionary);
  }

  static Future<List<Word>?> getDictionary(login) async {
    String baseUrl = usersUrl + '/' + login + "/dictionary";
    if (nextPageToken != "") {
      baseUrl += "?pageToken=" + nextPageToken;
    }
    final resp = await http.get(Uri.parse(baseUrl));

    Map jsonDecoded = json.decode(resp.body);

    if (jsonDecoded.containsKey("nextPageToken")) {
      nextPageToken = jsonDecoded["nextPageToken"];
    } else {
      nextPageToken = "";
    }

    List jsonWordList = json.decode(resp.body)["documents"];
    List<Word> retorno =
        jsonWordList.map((jsonObj) => Word.fromJson(jsonObj)).toList();
    return retorno;
  }

  static Future<User?> getUser(login) async {
    final resp = await http.get(Uri.parse(usersUrl + '/' + login));
    Map<String, dynamic> jsonUser = json.decode(resp.body);
    return User.fromJson(jsonUser);
  }

  static void createNewLocalUser(String name, String lastname, String residence,
      String birthdate, bool duo) {
    user = User(
        residenceCountry: residence,
        nativeLanguage: '',
        lastname: lastname,
        name: name,
        email: '',
        birthDate: birthdate,
        duolingo: duo,
        learnedLanguages: []);
  }

  static void setEmail(String email) {
    user!.email = email;
  }

  static void setNative(String native) {
    user!.nativeLanguage = native;
  }

  static void setLearn(List<String> learn) {
    user!.learnedLanguages = learn;
  }

  static void postUser(displayError) async {
    try {
      final resp = await http.post(Uri.parse(herokuUrl + '/users/'),
          body: json.encode(user),
          headers: {'Content-Type': 'application/json', 'charset': 'utf-8'});
    } catch (e) {
      displayError(
          "Your sign up request could not be completed, please check your connectivity and try again");
    }
  }

  static void clearCache() {
    user = null;
    dictionary = [];
    mapDetail = {};
  }
}
