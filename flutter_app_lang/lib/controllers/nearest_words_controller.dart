import 'dart:convert';
import 'package:latlong2/latlong.dart';
import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;

class NearestWordsController {
  static double latitude = 0;
  static double longitude = 0;
  static LatLng latestConnLocation = LatLng(latitude, longitude);
  static List<String>? latestRetrieved;

  static Future<bool> retrieveLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }

    _locationData = await location.getLocation();
    latitude = _locationData.latitude!;
    longitude = _locationData.longitude!;
    return true;
  }

  static void getNearestWords(
      user, latitude, longitude, setNearest, displayError) async {
    Map body = {"user": user, "latitude": latitude, "longitude": longitude};
    List<String> nearestWords = [];
    try {
      final req = await http.post(
        Uri.parse(herokuUrl + "/nearest"),
        body: json.encode(body),
        headers: {'Content-Type': 'application/json', 'charset': 'utf-8'},
      );
      List<dynamic> res = json.decode(req.body);
      nearestWords = (res.map((e) => e["word"] as String).toList());
      latestConnLocation = LatLng(latitude, longitude);
      latestRetrieved = nearestWords;
      setNearest(nearestWords);
    } catch (e) {
      getNearestWordsOffline(
          user, latitude, longitude, setNearest, displayError);
    }
  }

  static void getNearestWordsOffline(
      user, latitude, longitude, setNearest, displayError) async {
    List<String> nearestWords = [];
    if (latestRetrieved == null) {
      displayError(
          "The nearest words could not be retrieved, check your connectivity and try again");
      setNearest(nearestWords);
    } else {
      const Distance distance = Distance();
      if (distance(latestConnLocation, LatLng(latitude, longitude)) <= 50) {
        nearestWords = latestRetrieved!;
      }
      displayError(
          "The nearest words shown may not be up to date due to the lack of connectivity");
      setNearest(nearestWords);
    }
  }

  static void clearCache() {
    latitude = 0;
    longitude = 0;
    latestConnLocation = LatLng(latitude, longitude);
    latestRetrieved = null;
  }
}
