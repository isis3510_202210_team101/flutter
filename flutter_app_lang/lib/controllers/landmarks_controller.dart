import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:flutter_app_lang/models/models.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LandmarksController {
  static List possibleWords = [];
  static Landmark? newLandmark;
  static bool detailNewLandmark = false;
  static List<Landmark> listLandmarks = [];
  static Map<String, bool> mapDetail = {};
  static String nextPageToken = "";
  static String cachePopular = "";

  static void resetLandmarks() {
    listLandmarks = [];
  }

  static void resetDetail() {
    dynamic landmark;
    for (int i = 0; i < listLandmarks.length; i++) {
      landmark = listLandmarks[i];
      mapDetail[landmark.englishName] = false;
    }
  }

  static void retrieveLandmarksList(
      login, nativeLang, mainLearningLang, setLandmarks) {
    if (listLandmarks.isEmpty) {
      getLandmarksUser(login).then((respLandmarks) {
        listLandmarks = respLandmarks!;
        resetDetail();
        retrieveDetails(listLandmarks).then((value) {
          dynamic landmark;
          for (int i = 0; i < listLandmarks.length; i++) {
            landmark = listLandmarks[i];
            mapDetail[landmark.englishName] = false;
          }

          resetDetail();
          setLandmarks();
        });
      }).catchError((a) {
        listLandmarks = [];
        resetDetail();
      });
    } else {
      setLandmarks();
    }
  }

  static Future<List<Landmark>?> getLandmarksUser(login) async {
    String baseUrl = usersUrl + '/' + login + "/visitedLandmarks";
    if (nextPageToken != "") {
      baseUrl += "?pageToken=" + nextPageToken;
    }
    final resp = await http.get(Uri.parse(baseUrl));

    Map jsonDecoded = json.decode(resp.body);
    if (jsonDecoded.containsKey("nextPageToken")) {
      nextPageToken = jsonDecoded["nextPageToken"];
    } else {
      nextPageToken = "";
    }

    List jsonLandmarksList = json.decode(resp.body)["documents"];
    List<Landmark> returnLandmark =
        jsonLandmarksList.map((jsonObj) => Landmark.fromJson(jsonObj)).toList();
    return returnLandmark;
  }

  static void getPopularLandmark(setter, error) async {
    String baseUrl = landmarkStatisticsUrl + "/popular";
    try {
      final resp = await http.get(Uri.parse(baseUrl));
      Map jsonDecoded = json.decode(resp.body);
      Map jsonLandmarksList = json.decode(resp.body)["fields"];
      List<String> returnLandmark = jsonLandmarksList["first"]["referenceValue"].split("/");
      String returno = returnLandmark[returnLandmark.length-1];
      cachePopular = returno;
      setter(cachePopular);
    } catch (e) {
      if (cachePopular == "") {
        error("Please check your connectivity and try again");
      }
      else{
        setter(cachePopular);
      }
    }
  }

  static Future retrieveDetails(List<Landmark> listLandmarks) async {
    List<Future> futures = <Future>[];
    for (int i = 0; i < listLandmarks.length; i++) {
      futures.add(getLandmarkDetails(listLandmarks[i]));
    }
    await Future.wait(futures);
  }

  static Future<Landmark?> getLandmarkDetails(landmark) async {
    final resp =
        await http.get(Uri.parse(landmarksUrl + "/" + landmark.englishName));

    if (resp.statusCode == 200) {
      Map<String, dynamic> jsonDetails = json.decode(resp.body);
      landmark.originalLanguage =
          jsonDetails["fields"]["originalLanguage"]["stringValue"];
      landmark.originalName =
          jsonDetails["fields"]["originalName"]["stringValue"];
      landmark.visits =
          int.parse(jsonDetails["fields"]["visits"]["integerValue"]);
      landmark.description =
          jsonDetails["fields"]["description"]["stringValue"];
    }
  }

  static Future appendLandmarksList(
      login, nativeLang, mainLearningLang, setLandmarks) async {
    getLandmarksUser(login).then((respLandmarks) {
      listLandmarks.addAll(respLandmarks!);
      resetDetail();
      dynamic landmark;
      for (int i = 0; i < listLandmarks.length; i++) {
        landmark = listLandmarks[i];
        mapDetail[landmark.englishName] = false;
      }
      setLandmarks();
    }).catchError((a) {
      listLandmarks = [];
      resetDetail();
    });
  }

  static Future<int> createLandmark(urlFireStore) async {
    final body = {
      "image": {
        "source": {"imageUri": urlFireStore}
      },
      "user": Preferences.username
    };
    final resp = await http.post(
      Uri.parse(herokuUrl + "/landmarkDetection"),
      body: json.encode(body),
      headers: {"Content-Type": "application/json"},
    );
    if (resp.statusCode == 200) {
      final objResp = jsonDecode(resp.body)[0];
      newLandmark = Landmark(
          englishName: objResp["englishName"],
          originalLanguage: objResp["originalLanguage"],
          originalName: objResp["originalName"],
          visits: 0,
          date: "",
          description: objResp["description"],
          photo: urlFireStore);
    }
    return resp.statusCode;
  }

  static void clearCache() {
    possibleWords = [];
  }
}
