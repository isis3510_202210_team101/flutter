const String traslationsUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/translations";
const String traslationsDescriptionUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/translationsDescription";
const String languagesUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/languages";
const String usersUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/users";
const String landmarksUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/landmarks";
const String smartFeatureStatisticsUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/smartFeatureStatistics";
const String landmarkStatisticsUrl =
    "https://firestore.googleapis.com/v1/projects/biz-lang/databases/(default)/documents/statisticsLandmarks";
const String baseFireStorage =
    "https://firebasestorage.googleapis.com/v0/b/biz-lang.appspot.com/o";
const String herokuUrl = "https://lang-app-moviles-t10.herokuapp.com";
