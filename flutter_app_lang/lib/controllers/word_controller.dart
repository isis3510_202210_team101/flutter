import 'dart:convert';
import 'dart:io';

import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;

class WordController {
  static List possibleWords = [];
  static Map<String, bool> wordsToCreate = {};

  static Map createWordBody = {
    "user": Preferences.username,
    "learningLanguage": "",
    "word": " ",
    "photo": "",
    "latitude": 0,
    "longitude": 0
  };

  static void resetCreateWordBody() {
    createWordBody = {
      "user": Preferences.username,
      "learningLanguage": "",
      "word": " ",
      "photo": "",
      "latitude": 0,
      "longitude": 0
    };
  }

  static void resetWordsToAdd() {
    possibleWords = [];
    wordsToCreate = {};
  }

  static Future<String> determineLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return "";
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return "";
      }
    }

    _locationData = await location.getLocation();
    createWordBody["latitude"] = _locationData.latitude;
    createWordBody["longitude"] = _locationData.longitude;
    return "";
  }

  static Future<String> uploadImage(pickedFile) async {
    final name = DateTime.now().millisecondsSinceEpoch.toString();
    final postUri =
        Uri.parse(baseFireStorage + "?uploadType=media&name=" + name);
    final mpFile = http.MultipartFile.fromBytes(
        'picture', File(pickedFile.path).readAsBytesSync(),
        filename: name);
    final request = http.MultipartRequest("POST", postUri);
    request.headers.addAll({"Content-Type": "image/jpeg"});
    request.files.add(mpFile);

    final mapResponse =
        json.decode(await ((await request.send()).stream.bytesToString()));

    return baseFireStorage +
        "/" +
        name +
        "?alt=media&token=" +
        mapResponse["downloadTokens"];
  }

  static Future<int> imageRecognition(
      photo, nativeLocale, objectiveLocale, startTime, updateWords) async {
    Map body = {
      "image": {
        "source": {"imageUri": photo}
      },
      "native": nativeLocale,
      "objective": objectiveLocale
    };
    final response_1 = await http.post(
      Uri.parse(herokuUrl + "/objectDetection"),
      body: json.encode(body),
      headers: {"Content-Type": "application/json"},
    );
    possibleWords = json.decode(response_1.body);
    for (int i = 0; i < possibleWords.length; i++) {
      wordsToCreate[possibleWords[i]["objective"]] = false;
    }
    final endTime = DateTime.now().millisecondsSinceEpoch - startTime;
    String currentDate = DateTime.now().toString().replaceAll(' ', 'T');
    currentDate = currentDate.substring(0, currentDate.indexOf('.')) + "Z";
    final response_2 = await http.post(Uri.parse(smartFeatureStatisticsUrl),
        body: json.encode({
          "fields": {
            "elapsedTime": {"doubleValue": endTime},
            "date": {"timestampValue": currentDate}
          }
        }));
    updateWords();
    return response_2.statusCode;
  }

  static Future<int> createWord() async {
    await determineLocation();
    final response = await http.post(Uri.parse(herokuUrl + "/words"),
        body: json.encode(createWordBody),
        headers: {"Content-Type": "application/json"});
    return response.statusCode;
  }

  static Future<int> createWords(List wordsToCreate) async {
    int status = 200;
    for (int i = 0; i < wordsToCreate.length; i++) {
      resetCreateWordBody();
      createWordBody["learningLanguage"] = Preferences.currentLang;
      createWordBody["word"] = wordsToCreate[i];
      int localStatus = await createWord();
      if (localStatus != 200 && localStatus != 400) {
        status = localStatus;
        break;
      }
    }
    return status;
  }

  static Future<int> deleteWordBackend(username, word) async {
    Map body = {"user": username, "englishWord": word};
    final response = await http.delete(Uri.parse(herokuUrl + "/words/user"),
        body: json.encode(body), headers: {"Content-Type": "application/json"});
    return response.statusCode;
  }

  static void clearCache() {
    possibleWords = [];
    wordsToCreate = {};
  }
}
