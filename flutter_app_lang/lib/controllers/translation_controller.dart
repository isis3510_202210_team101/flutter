import 'dart:convert';
import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:flutter_app_lang/models/models.dart';
import 'package:http/http.dart' as http;

class TranslationController {
  static List<Word> words = [];

  static Future retrieveTranslations(
      List<Word> wordsDictionary, nativeLang, mainLearningLang) async {
    List<Future> futures = <Future>[];
    for (int i = 0; i < wordsDictionary.length; i++) {
      futures.add(
          getWordTranslation(wordsDictionary[i], nativeLang, mainLearningLang));
    }
    await Future.wait(futures);
  }

  static Future getDescription(wordObj, mainLearningLang, setWordsDictionary) async {
    Word currentWord = wordObj;
    String word = currentWord.idWord;
    final resp = await http.get(
        Uri.parse(traslationsDescriptionUrl + "/" + mainLearningLang + "-" + word));

    if (resp.statusCode == 200) {
      String description =
          json.decode(resp.body)["fields"]["description"]["stringValue"];
      currentWord.description = description;
      setWordsDictionary();
    }
  }

  static Future<Word> getWordTranslation(
      wordObj, nativeLang, mainLearningLang) async {
    Word currentWord = wordObj;
    String word = currentWord.idWord;
    Translation? wordTranslated = await getTranslation(word, nativeLang);
    if (wordTranslated != null) {
      currentWord.originalWord = wordTranslated.translation;
    }
    wordTranslated = await getTranslation(word, mainLearningLang);
    if (wordTranslated != null) {
      currentWord.translation = wordTranslated.translation;
    }
    return currentWord;
  }

  static Future<Translation?> getTranslation(word, language) async {
    final resp =
        await http.get(Uri.parse(traslationsUrl + "/" + language + "-" + word));

    if (resp.statusCode == 200) {
      Map<String, dynamic> jsonTranslation = json.decode(resp.body);
      return Translation.fromJson(jsonTranslation);
    } else {
      return null;
    }
  }

  static void clearCache() {
    words = [];
  }
}
