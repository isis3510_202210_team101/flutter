import 'dart:convert';

import 'package:flutter_app_lang/controllers/firestore_urls.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app_lang/models/language.dart';

class LanguagesController {
  static List<Language> languages = [];
  static List<String> userLanguages = [];

  static void retrieveLanguages(setLanguages) {
    if (languages.isEmpty) {
      getLanguages().then((respList) {
        languages = respList;
        setLanguages(languages);
      });
    }
    else{
      setLanguages(languages);
    }
  }

  static void retrieveForgottenLanguages(username, setForgottenLanguages) {
    List<dynamic> languages = [];
    http.post(
      Uri.parse(herokuUrl + "/forgot"),
      body: json.encode({"user": username}),
      headers: {'Content-Type': 'application/json', 'charset': 'utf-8'},
    ).then((listLanguages) {
      return {
        languages = json.decode(listLanguages.body),
        setForgottenLanguages(languages)
      };
    });
  }

  static Future<String> getLocale(language) async {
    final resp = await http.get(Uri.parse(languagesUrl + "/" + language));
    return json.decode(resp.body)["fields"]["locale"]["stringValue"];
  }

  static Future<List<Language>> getLanguages() async {
    final resp = await http.get(Uri.parse(languagesUrl));
    List jsonLangList = json.decode(resp.body)["documents"];
    return jsonLangList.map((jsonObj) => Language.fromJson(jsonObj)).toList();
  }

  static Future<Language> getLanguage(lang) async {
    final resp = await http.get(Uri.parse(languagesUrl + '/' + lang));
    Map<String, dynamic> jsonUser = json.decode(resp.body);
    return Language.fromJson(jsonUser);
  }

  static void clearCache() {
    languages = [];
    userLanguages = [];
  }
}


// final allData = querySnapshot.docs.map((doc) => doc.data()).toList();
