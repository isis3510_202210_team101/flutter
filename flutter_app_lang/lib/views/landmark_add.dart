import 'package:flutter/material.dart';
import 'package:flutter_app_lang/components/langCard.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/components/clickableIcon.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as app_colors;

class LandmarkAdd extends StatefulWidget {
  const LandmarkAdd({Key? key}) : super(key: key);

  @override
  State<LandmarkAdd> createState() => _LandmarkAddState();
}

class _LandmarkAddState extends State<LandmarkAdd> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add landmark words"),
      ),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
            LandmarksController.newLandmark != null
                ? LangCard(
                    color: app_colors.landmarkContainer,
                    highlightColor: app_colors.landmarkContainer,
                    title: LandmarksController.newLandmark!.originalName,
                    subtitle: LandmarksController.newLandmark!.englishName,
                    leadingIcon: showFlag1(
                        LandmarksController.newLandmark!.originalLanguage),
                    trailIcon: ClickableIcon(
                        iconOff:
                            Icon(Icons.expand_more, color: app_colors.landmark),
                        iconOn:
                            Icon(Icons.expand_less, color: app_colors.landmark),
                        type: TypeOfClickableButton.unique,
                        onTap: () {
                          LandmarksController.detailNewLandmark =
                              !LandmarksController.detailNewLandmark;
                        }),
                    onTap: () {
                      LandmarksController.detailNewLandmark =
                          !LandmarksController.detailNewLandmark;
                    },
                    bottom: Column(children: <Widget>[
                      LandmarksController.newLandmark!.photo != ""
                          ? CachedNetworkImage(
                              imageUrl: LandmarksController.newLandmark!.photo,
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error),
                              width: 500,
                              fit: BoxFit.cover,
                            )
                          : Image.asset("assets/no_image.jpg",
                              width: 500, fit: BoxFit.cover),
                      LandmarksController.detailNewLandmark
                          ? Padding(
                              padding: const EdgeInsets.all(16),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(LandmarksController
                                      .newLandmark!.description)))
                          : Container(),
                    ]),
                  )
                : Container(),
            Container(
              height: 25,
            ),
            const Text("Select the words you want to add to your dictionary:"),
            ...WordController.possibleWords
                .map((possibleWord) => LangCard(
                    color: app_colors.surfaceVariant,
                    highlightColor: app_colors.landmarkContainer,
                    title: possibleWord["objective"],
                    subtitle: possibleWord["native"],
                    onTap: () {
                      WordController.wordsToCreate[possibleWord["objective"]] =
                          !WordController
                              .wordsToCreate[possibleWord["objective"]]!;
                    }))
                .toList(),
            LangCard(
              color: app_colors.surfaceVariant,
              highlightColor: app_colors.landmarkContainer,
              title: "None of the above",
              onTap: () {
                Navigator.pushReplacementNamed(context, 'landmarks');
              },
            ),
            Container(
              height: 25,
            )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.check_rounded),
        onPressed: () {
          Connectivity().checkConnectivity().then((result) {
            if (result == ConnectivityResult.none) {
              NotificationsService.showSnackbar(
                  "Check your internet connectivity to upload the new words to your dictionary.");
            } else {
              List wordsToCreate = [];
              List<String> keys = WordController.wordsToCreate.keys.toList();
              for (int i = 0; i < keys.length; i++) {
                if (WordController.wordsToCreate[keys[i]]!) {
                  wordsToCreate.add(keys[i]);
                }
              }
              if (wordsToCreate.isNotEmpty) {
                WordController.createWords(wordsToCreate).then((statusCode) {
                  if (statusCode == 200) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Success'),
                            content: const Text(
                                'The words were added successfully to your dictionary.'),
                            actions: <Widget>[
                              TextButton(
                                  child: const Text("OK"),
                                  onPressed: () {
                                    WordController.resetWordsToAdd();
                                    Navigator.pushReplacementNamed(
                                        context, 'landmarks');
                                  })
                            ],
                          );
                        });
                  } else if (statusCode == 404) {
                    NotificationsService.showSnackbar(
                        "You must provide a valid word in the selected language.");
                  } else {
                    NotificationsService.showSnackbar(
                        "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                  }
                }).catchError((error) {
                  NotificationsService.showSnackbar(
                      "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                });
              } else {
                NotificationsService.showSnackbar(
                    "You must select at least one of the words in order to add it.");
              }
            }
          });
        },
      ),
    );
  }
}

showFlag1(String s) {
  switch (s) {
    case "German":
      return const Image(
        image: AssetImage("assets/flags/1f1e9-1f1ea.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Spanish":
      return const Image(
        image: AssetImage("assets/flags/1f1ea-1f1f8.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Italian":
      return const Image(
        image: AssetImage("assets/flags/1f1ee-1f1f9.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Portuguese":
      return const Image(
        image: AssetImage("assets/flags/1f1f5-1f1f9.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "English":
      return const Image(
        image: AssetImage("assets/flags/1f1fa-1f1f8.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    default:
      return const Image(
        image: AssetImage("assets/flags/1f30e.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
  }
}
