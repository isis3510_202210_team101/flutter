import 'dart:async';
import 'package:flutter_app_lang/components/streakCard.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lang/components/navbar.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'package:flutter_app_lang/components/roundedSquareCard.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> nearestWords = [];
  List<dynamic> forgottenLanguages = [];
  bool first = true;
  bool? location;
  StreamSubscription? connection;
  bool isOffline = true;

  void setNearestWords(List<String> receivedNearestWords) {
    nearestWords = receivedNearestWords;
    if (mounted) {
      setState(() {});
    }
  }

  void setForgottenLanguages(List<dynamic> receiveForgottenLanguages) {
    forgottenLanguages = receiveForgottenLanguages;
    if (mounted) {
      setState(() {});
    }
  }

  void displayError(String message) {
    if (mounted) {
      NotificationsService.showSnackbar(message);
    }
  }

  @override
  void initState() {
    if (Preferences.lastAccess == "") {
      Preferences.streak = 1;
    } else {
      final lastDateTime = DateTime.parse(Preferences.lastAccess);
      final nowDateTime = DateTime.now();
      final difference = lastDateTime.difference(nowDateTime).inDays;
      if (difference == 1) {
        Preferences.streak = Preferences.streak + 1;
      } else {
        Preferences.streak = 1;
      }
    }
    Preferences.lastAccess = DateFormat('yyyy-MM-dd').format(DateTime.now());
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    bool wasOffline = false;
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      // whenever connection status is changed.
      if (result == ConnectivityResult.none) {
        displayError("You are offline");
        //there is no any connection
        setState(() {
          isOffline = true;
          wasOffline = true;
        });
      } else {
        //there is connection
        if (wasOffline) {
          displayError("You are back online");
        }
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    String title = "";
    String subtitle = "";
    if (first) {
      first = false;
      NearestWordsController.retrieveLocation().then((gotLocation) {
        if (gotLocation && !isOffline) {
          location = true;
          NearestWordsController.getNearestWords(
              Preferences.username,
              NearestWordsController.latitude,
              NearestWordsController.longitude,
              setNearestWords,
              displayError);
        } else if (gotLocation && isOffline) {
          location = true;
          NearestWordsController.getNearestWordsOffline(
              Preferences.username,
              NearestWordsController.latitude,
              NearestWordsController.longitude,
              setNearestWords,
              displayError);
        } else {
          location = false;
          setState(() {});
        }
      });
    }
    if (location == null) {
      title = "Loading...";
    } else if (!location!) {
      title = "Please enable location services";
      subtitle = "In order to get GPS based word recommendations";
    } else {
      switch (nearestWords.length) {
        case 0:
          {
            title = "No words were saved";
            subtitle = "Let's add a new word";
          }
          break;
        case 1:
          {
            title = nearestWords[0];
          }
          break;
        case 2:
          {
            title = nearestWords[0] + "\n" + nearestWords[1];
          }
          break;
        default:
          {
            title = nearestWords[0] + "\n" + nearestWords[1];
            subtitle = "+ ";
            subtitle += (nearestWords.length - 2).toString();
            subtitle += " more words";
          }
          break;
      }
    }

    if (!isOffline) {
      LanguagesController.retrieveForgottenLanguages(
          Preferences.username, setForgottenLanguages);
    }

    int difference;

    if (Preferences.lastLandmark == "") {
    } else {
      final lastDateTime = DateTime.parse(Preferences.lastLandmark);
      final nowDateTime = DateTime.now();
      difference = lastDateTime.difference(nowDateTime).inDays;
    }

    // Test

    difference = 32;
    Preferences.streak = 10;

    Color color = app_colors.tertiaryContainer;
    if (Preferences.streak > 10) {
      color = const Color(0xffffb4a9);
    } else if (Preferences.streak > 4) {
      color = app_colors.secondaryContainer;
    }
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'LangApp',
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          actions: [
            IconButton(
                icon: Image(
                  image:
                      AssetImage("assets/flags/" + Preferences.flag + ".png"),
                  height: 36,
                  fit: BoxFit.scaleDown,
                ),
                onPressed: () {
                  Connectivity().checkConnectivity().then((result) async {
                    if (result == ConnectivityResult.none) {
                      displayError(
                          "Check your internet connectivity before updating the language you want to learn.");
                    } else {
                      Navigator.pushNamed(context, 'changeLang');
                    }
                  });
                }),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: IconButton(
                  icon: const Icon(Icons.logout_outlined),
                  onPressed: () {
                    authService.logout();
                    NearestWordsController.clearCache();
                    LanguagesController.clearCache();
                    TranslationController.clearCache();
                    UsersController.clearCache();
                    WordController.clearCache();
                    Navigator.pushReplacementNamed(context, 'welcome');
                  }),
            ),
          ],
        ),
        body: Center(
            child: ListView(children: <Widget>[
          Preferences.streak > 1
              ? StreakCard(
                  color: color,
                  mainText: "Current streak",
                  subText: Preferences.streak.toString() + " days in a row",
                  trailIcon: Icon(
                    Icons.local_fire_department,
                    size: 50,
                    color: color,
                  ))
              : Container(),
          forgottenLanguages.isNotEmpty
              ? RSCard(
                  color: app_colors.tertiaryContainer,
                  title:
                      "It's been a long time since you practiced these languages",
                  mainText: forgottenLanguages[0],
                  subText: forgottenLanguages.length > 1
                      ? forgottenLanguages[1]
                      : "",
                  child: ElevatedButton(
                    onPressed: null,
                    style: ElevatedButton.styleFrom(
                      primary: app_colors.tertiary, // Background color
                      onPrimary: app_colors
                          .onTertiary, // Text Color (Foreground color)
                      elevation: 0,
                    ),
                    child: Text('Practice ' + forgottenLanguages[0]),
                  ))
              : Container(),
          (difference >= 30)
              ? StreakCard(
                  color: app_colors.landmark,
                  mainText: "Capture landmarks",
                  subText: "to learn new words",
                  trailIcon: Icon(
                    Icons.local_see_rounded,
                    size: 50,
                    color: app_colors.landmark,
                  ))
              : Container(),
          RSCard(
              color: app_colors.tertiaryContainer,
              title: "Last time you were here...",
              mainText: title,
              subText: subtitle,
              child: ElevatedButton(
                onPressed: null,
                style: ElevatedButton.styleFrom(
                  primary: app_colors.tertiary, // Background color
                  onPrimary:
                      app_colors.onTertiary, // Text Color (Foreground color)
                  elevation: 0,
                ),
                child: const Text('Start flashcard session'),
              )),
          Container(
            height: 25,
          )
        ])),
        bottomNavigationBar: const Navbar());
  }
}
