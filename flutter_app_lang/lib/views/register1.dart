import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/providers/signup_form_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as colors;
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_fonts/google_fonts.dart';

class RegisterView1 extends StatelessWidget {
  const RegisterView1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 90),
            child: Column(
              children: [
                Column(
                  children: [
                    Text('Welcome to',
                        style: Theme.of(context).textTheme.headline6),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: Text('LangApp',
                            style: Theme.of(context).textTheme.headline2)),
                    Text('Enter your information',
                        style: Theme.of(context).textTheme.headline6),
                    ChangeNotifierProvider(
                        create: (_) => SignUpProvider(),
                        child: const SignUpForm())
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SignUpForm();
  }
}

class _SignUpForm extends State<SignUpForm> {
  TextEditingController dateinput = TextEditingController();

  String? _selectedValue;
  List _items = [];
  bool? duo;

  Future<void> readJson() async {
    final resBody =
        json.decode(await rootBundle.loadString('assets/countries.json'));
    setState(() {
      _items = resBody;
    });
  }

  @override
  void initState() {
    dateinput.text = '';
    readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final signUpForm = Provider.of<SignUpProvider>(context);

    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 50),
        child: Form(
          key: signUpForm.formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: TextFormField(
                    style: const TextStyle(fontSize: 15),
                    initialValue:
                        signUpForm.name == '' ? null : signUpForm.name,
                    onChanged: (value) => signUpForm.name = value,
                    validator: (value) {
                      if (value == null || value.isEmpty || value == '') {
                        return 'This field is required.';
                      }
                    },
                    maxLength: 20,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        labelText: 'Name', border: OutlineInputBorder()),
                  )),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: TextFormField(
                  style: const TextStyle(fontSize: 15),
                  initialValue:
                      signUpForm.lastname == '' ? null : signUpForm.lastname,
                  onChanged: (value) => signUpForm.lastname = value,
                  validator: (value) {
                    if (value == null || value.isEmpty || value == '') {
                      return 'This field is required.';
                    }
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  maxLength: 20,
                  decoration: const InputDecoration(
                      labelText: 'Lastname', border: OutlineInputBorder()),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: DropdownButtonFormField(
                    style: const TextStyle(fontSize: 15, color: Colors.black),
                    items: _items.map((item) {
                      return DropdownMenuItem(
                        child: Text(item['name']),
                        value: item['name'],
                      );
                    }).toList(),
                    onChanged: (newVal) {
                      setState(() {
                        _selectedValue = newVal! as String?;
                        signUpForm.country = _selectedValue!;
                      });
                    },
                    validator: (value) {
                      if (value == null || value == '') {
                        return 'This field is required.';
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        labelStyle: TextStyle(fontSize: 15),
                        labelText: 'Country of residence',
                        border: OutlineInputBorder())),
              ),
              Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                  child: TextField(
                    style: const TextStyle(fontSize: 15),
                    controller: dateinput,
                    decoration: const InputDecoration(
                        suffixIcon: Icon(Icons.calendar_today),
                        labelText: 'Date of birth',
                        border: OutlineInputBorder()),
                    readOnly: true,
                    minLines: 1,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime(2015),
                          firstDate: DateTime(1970),
                          lastDate: DateTime(2015));

                      if (pickedDate != null) {
                        String formattedDate =
                            DateFormat('yyyy-MM-dd').format(pickedDate);
                        signUpForm.birthDate = formattedDate;
                        setState(() {
                          dateinput.text =
                              formattedDate; //set output date to TextField value.
                        });
                      }
                    },
                  )),
              ListTile(
                title: Text(
                  "I am a former/current Duolingo user",
                  style: GoogleFonts.oxygen(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0),
                ),
                leading: Radio<bool>(
                  value: true,
                  groupValue: duo,
                  toggleable: true,
                  visualDensity:
                      const VisualDensity(horizontal: -4, vertical: 0),
                  onChanged: (bool? value) {
                    setState(() {
                      duo = value;
                    });
                  },
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(children: [
                        OutlinedButton.icon(
                          onPressed: () => Navigator.pushReplacementNamed(
                              context, 'welcome'),
                          icon: const Icon(Icons.arrow_back),
                          label: const Text(""),
                        )
                      ]),
                      const Spacer(),
                      Column(children: [
                        ElevatedButton(
                            child: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 80),
                                child: const Text(
                                  'Next',
                                  style: TextStyle(color: Colors.white),
                                )),
                            onPressed: signUpForm.isLoading
                                ? null
                                : () async {
                                    FocusScope.of(context).unfocus();
                                    if (!signUpForm.isValidForm()) return;
                                    signUpForm.isLoading = true;
                                    bool duolingo = duo == null ? false : true;
                                    UsersController.createNewLocalUser(
                                        signUpForm.name,
                                        signUpForm.lastname,
                                        signUpForm.country,
                                        signUpForm.birthDate,
                                        duolingo);
                                    Navigator.pushReplacementNamed(
                                        context, 'onboardingNL');
                                  })
                      ])
                    ],
                  )),
              Container(
                height: 40,
              )
            ],
          ),
        ));
  }
}
