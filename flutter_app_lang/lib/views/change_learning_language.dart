import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/services/notifications_service.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'package:flutter_app_lang/components/langCard.dart';

class ChangeLL extends StatefulWidget {
  const ChangeLL({Key? key}) : super(key: key);

  @override
  State<ChangeLL> createState() => _ChangeLLState();
}

class _ChangeLLState extends State<ChangeLL> {
  List<Widget> content = [];
  List languages = [];
  bool first = true;
  static Map<String, bool> mapSelected = {};

  void displayError(String message) {
    if (mounted) {
      NotificationsService.showSnackbar(message);
    }
  }

  void setLanguages(List receivedLanguages) {
    for (int i = 0; i < receivedLanguages.length - 1; i++) {
      LanguagesController.getLanguage(receivedLanguages[i]).then((lang) {
        languages.add(lang);
        if (receivedLanguages[i] == Preferences.currentLang) {
          mapSelected[Preferences.currentLang] = true;
        } else {
          mapSelected[receivedLanguages[i]] = false;
        }
        if (mounted) {
          setState(() {});
        }
      });
    }
  }

  Widget generateCards(element) {
    AssetImage flag;
    try {
      flag = AssetImage("assets/flags/" + element.flag + ".png");
    } on Exception {
      flag = const AssetImage("assets/flags/1f30e.png");
    }
    return LangCard(
      color: mapSelected[element.language] == true
          ? app_colors.primaryContainer
          : app_colors.surfaceVariant,
      highlightColor: mapSelected[element.language] == true
          ? app_colors.primaryContainer
          : app_colors.surfaceVariant,
      title: element.language,
      subtitle: element.englishLanguage,
      trailIcon: Image(
        image: flag,
        height: 30,
        width: 36,
        fit: BoxFit.scaleDown,
      ),
      onTap: () {
        Connectivity().checkConnectivity().then((result) async {
          if (result == ConnectivityResult.none) {
            displayError(
                "Check your internet connectivity before updating the language you want to learn.");
          } else {
            Preferences.currentLang = element.englishLanguage;
            Preferences.flag = element.flag;
            WordController.clearCache();
            UsersController.clearCache();
            TranslationController.clearCache();
            for (String a in mapSelected.keys) {
              mapSelected[a] = false;
            }
            mapSelected[element.language] = true;
            setState(() {});
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (first) {
      first = false;
      UsersController.retrieveUser(Preferences.username, setLanguages);
    }
    content = languages.map((element) => generateCards(element)).toList();
    content.add(Container(
      height: 25,
    ));

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Select your language',
        ),
        actions: const [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.search_rounded),
          ),
        ],
      ),
      body: Center(child: ListView(children: content)),
    );
  }
}
