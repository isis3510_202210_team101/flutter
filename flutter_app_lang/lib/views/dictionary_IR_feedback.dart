import "package:flutter/material.dart";
import 'package:flutter_app_lang/components/langCard.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as app_colors;

class DictionaryIRFeedbackView extends StatefulWidget {
  const DictionaryIRFeedbackView({Key? key}) : super(key: key);

  @override
  State<DictionaryIRFeedbackView> createState() =>
      _DictionaryIRFeedbackViewState();
}

class _DictionaryIRFeedbackViewState extends State<DictionaryIRFeedbackView> {
  bool seleccionado = false;

  void updateWords() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    final startTime = DateTime.now().millisecondsSinceEpoch;
    LanguagesController.getLocale(
            WordController.createWordBody["learningLanguage"])
        .then((objectiveLocale) {
      LanguagesController.getLocale(Preferences.nativeLang)
          .then((nativeLocale) {
        WordController.imageRecognition(WordController.createWordBody["photo"],
                nativeLocale, objectiveLocale, startTime, updateWords)
            .catchError((error) {
          NotificationsService.showSnackbar(
              "A problem occur when getting the objects in the image. Check your connectivity and try again.");
        });
      });
    }).catchError((error) {
      NotificationsService.showSnackbar(
          "A problem occur validating your language. Check your connectivity and try again.");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add new word"),
      ),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: [
            const Text("Select the word you want to add to your dictionary:"),
            ...WordController.possibleWords
                .map((possibleWord) => LangCard(
                    color: app_colors.surfaceVariant,
                    highlightColor: app_colors.primaryContainer,
                    title: possibleWord["objective"],
                    subtitle: possibleWord["native"],
                    onTap: () {
                      if (!seleccionado) {
                        seleccionado = true;
                        Connectivity().checkConnectivity().then((result) {
                          if (result == ConnectivityResult.none) {
                            NotificationsService.showSnackbar(
                                "Check your internet connectivity to upload a new word to your dictionary.");
                          } else {
                            WordController.createWordBody["word"] =
                                possibleWord["objective"];
                            WordController.createWord().then((statusCode) {
                              if (statusCode == 200) {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text('Success'),
                                        content: Text('The word ' +
                                            WordController
                                                .createWordBody['word'] +
                                            ' was added successfully to your dictionary.'),
                                        actions: <Widget>[
                                          TextButton(
                                              child: const Text("OK"),
                                              onPressed: () {
                                                UsersController.dictionary = [];
                                                WordController
                                                    .resetWordsToAdd();
                                                WordController
                                                    .resetCreateWordBody();
                                                Navigator.pop(context);
                                                Navigator.pop(context);
                                                Navigator.pushReplacementNamed(
                                                    context, 'dictionary');
                                              })
                                        ],
                                      );
                                    });
                              } else {
                                NotificationsService.showSnackbar(
                                    "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                              }
                            }).catchError((error) {
                              NotificationsService.showSnackbar(
                                  "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                            });
                          }
                        });
                      }
                    }))
                .toList(),
            LangCard(
              color: app_colors.surfaceVariant,
              highlightColor: app_colors.primaryContainer,
              title: "None of the above",
              onTap: () {
                Navigator.pushNamed(context, 'dictionary_add');
              },
            ),
            Container(
              height: 25,
            )
          ],
        ),
      )),
    );
  }
}
