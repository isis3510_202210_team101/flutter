import 'package:flutter/material.dart';
import 'package:flutter_app_lang/services/auth_service.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as colors;
import 'package:flutter_app_lang/views/views.dart';
import 'package:provider/provider.dart';

class WelcomeView extends StatelessWidget {
  const WelcomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    return Scaffold(
        backgroundColor: Colors.white,
        body: 
            _WelcomeScreen()
            /* FutureBuilder(
            future: authService.readToken(),
            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
              if (!snapshot.hasData) {
                return const Text('Loading');
              }

              if (snapshot.data == '') {
                Future.microtask(() => {
                      Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(
                              pageBuilder: (_, __, ___) => _WelcomeScreen(),
                              transitionDuration: const Duration(seconds: 0)))
                    });
              } else {
                Future.microtask(() => {
                      Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(
                              pageBuilder: (_, __, ___) => const Home(),
                              transitionDuration: const Duration(seconds: 0)))
                    });
              }
              return Container();
            }) */
          );
  }
}

class _WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 90),
            child: Column(
              children: [
                Column(
                  children: [
                    Text('Welcome to',
                        style: Theme.of(context).textTheme.headline6),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 40),
                        child: Text('LangApp',
                            style: Theme.of(context).textTheme.headline2)),
                    Text('Let\'s get started',
                        style: Theme.of(context).textTheme.headline6),
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 40),
                        child: Image.asset('assets/welcome.png')),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 30),
                        child: Column(children: [
                          ElevatedButton(
                              child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 57),
                                  child: const Text(
                                    'Login',
                                    style: TextStyle(color: Colors.black),
                                  )),
                              onPressed: () => Navigator.pushReplacementNamed(
                                  context, 'login'),
                              style: ElevatedButton.styleFrom(
                                primary: colors.secondaryContainer,
                                onPrimary: Colors.black,
                              ))
                        ])),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 90),
                        child: Column(children: [
                          OutlinedButton(
                            onPressed: () => Navigator.pushReplacementNamed(
                                context, 'signup1'),
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 50),
                              child: const Text("Sign up"),
                            ),
                          )
                        ])),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
