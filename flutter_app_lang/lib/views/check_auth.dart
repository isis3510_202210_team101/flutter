import 'package:flutter/material.dart';
import 'package:flutter_app_lang/services/auth_service.dart';
import 'package:flutter_app_lang/views/views.dart';
import 'package:provider/provider.dart';


class CheckAuth extends StatelessWidget {
  const CheckAuth({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    final authService = Provider.of<AuthService>(context, listen: false);

    return Scaffold(
      body: Center(
        child: FutureBuilder(
          future: authService.readToken(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            
            if ( !snapshot.hasData )            
              return const Text('');

            if ( snapshot.data == '' ) {
              Future.microtask(() {

                Navigator.pushReplacement(context, PageRouteBuilder(
                  pageBuilder: ( _, __ , ___ ) => const WelcomeView(),
                  transitionDuration: const Duration( seconds: 0)
                  )
                );

              });

            } else {

              Future.microtask(() {

                Navigator.pushReplacement(context, PageRouteBuilder(
                  pageBuilder: ( _, __ , ___ ) => const Home(),
                  transitionDuration: const Duration( seconds: 0)
                  )
                );

              });
            }

            return Container();

          },
        ),
     ),
   );
  }
}