import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'package:flutter_app_lang/components/langCard.dart';

class OnboardingNL extends StatefulWidget {
  const OnboardingNL({Key? key}) : super(key: key);

  @override
  State<OnboardingNL> createState() => _OnboardingNLState();
}

class _OnboardingNLState extends State<OnboardingNL> {
  List<Widget> content = [];
  List languages = [];
  static Map<String, bool> mapSelected = {};

  void setLanguages(List receivedLanguages) {
    languages = receivedLanguages;

    for (int i = 0; i < receivedLanguages.length; i++) {
      mapSelected[receivedLanguages[i].englishLanguage] = false;
    }
    setState(() {});
  }

  Widget generateCards(element) {
    AssetImage flag;
    try {
      flag = AssetImage("assets/flags/" + element.flag + ".png");
    } on Exception {
      flag = const AssetImage("assets/flags/1f30e.png");
    }
    return LangCard(
      color: mapSelected[element.language] == true ? app_colors.primaryContainer : app_colors.surfaceVariant,
      highlightColor: mapSelected[element.language] == true ? app_colors.primaryContainer : app_colors.surfaceVariant,
      title: element.language,
      trailIcon: Image(
        image: flag,
        height: 30,
        width: 36,
        fit: BoxFit.scaleDown,
      ),
      onTap: () {
        UsersController.setNative(element.englishLanguage);
        Preferences.nativeLang = element.englishLanguage;
        for (String a in mapSelected.keys) {
          mapSelected[a] = false;
        }
        mapSelected[element.language] = true;
        setState(() {});
        Navigator.pushNamed(context, 'onboardingLL');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    LanguagesController.retrieveLanguages(setLanguages);
    content = languages.map((element) => generateCards(element)).toList();
    content.add(Container(
      height: 25,
    ));

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Select your language',
        ),
        actions: const [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.search_rounded),
          ),
        ],
      ),
      body: Center(child: ListView(children: content)),
    );
  }

}
