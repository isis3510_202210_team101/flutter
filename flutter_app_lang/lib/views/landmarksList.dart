import 'package:flutter/material.dart';
import 'package:flutter_app_lang/components/clickableIcon.dart';
import 'package:flutter_app_lang/components/langCard.dart';
import 'package:flutter_app_lang/components/navbar.dart';
import 'package:flutter_app_lang/components/streakCard.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:image_picker/image_picker.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'dart:async';

class LandmarksView extends StatefulWidget {
  const LandmarksView({Key? key}) : super(key: key);

  @override
  State<LandmarksView> createState() => _LandmarksView();
}

class _LandmarksView extends State<LandmarksView> {
  StreamSubscription? connection;
  bool first = true;
  bool isOffline = false;
  final ScrollController scrollController = ScrollController();
  bool isLoading = false;
  String? popular;

  void setLandmarks() {
    if (mounted) {
      setState(() {});
    }
  }

  void resetLandmark(landmark) {
    LandmarksController.mapDetail[landmark.englishName] =
        !LandmarksController.mapDetail[landmark.englishName]!;
    setState(() {});
  }

  @override
  void initState() {
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          isOffline = true;
        });
      } else {
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });

    scrollController.addListener(() {
      if (scrollController.position.pixels + 500 >=
          scrollController.position.maxScrollExtent) {
        if (LandmarksController.listLandmarks.isEmpty ||
            LandmarksController.nextPageToken != "") {
          fetchData();
        }
      }
    });

    String username = Preferences.username;
    String nativeLang = Preferences.nativeLang;
    String mainLearningLang = Preferences.currentLang;
    LandmarksController.retrieveLandmarksList(
        username, nativeLang, mainLearningLang, setLandmarks);
    super.initState();
  }

  Future fetchData() async {
    if (isLoading) return;

    isLoading = true;
    setState(() {});

    String username = Preferences.username;
    String nativeLang = Preferences.nativeLang;
    String mainLearningLang = Preferences.currentLang;
    LandmarksController.appendLandmarksList(
            username, nativeLang, mainLearningLang, setLandmarks)
        .then((value) async {
      await Future.delayed(const Duration(seconds: 4));
      isLoading = false;
      setState(() {});
      if (scrollController.position.pixels + 100 >=
          scrollController.position.maxScrollExtent) {
        return;
      }
      scrollController.animateTo(scrollController.position.pixels + 120,
          duration: const Duration(milliseconds: 300),
          curve: Curves.fastOutSlowIn);
    });
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  void setPopular(String receivedPopular) {
    popular = receivedPopular;
    if (mounted) {
      setState(() {});
    }
  }

  void displayError(String message) {
    if (mounted) {
      NotificationsService.showSnackbar(message);
    }
  }

  @override
  Widget build(BuildContext context) {
    LandmarksController.getPopularLandmark(setPopular, displayError);
    if (LandmarksController.mapDetail.isEmpty) {
      LandmarksController.resetDetail();
    }
    final size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Landmarks"),
          actions: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: isOffline
                    ? const Icon(Icons.cloud_off)
                    : const Icon(Icons.cloud_done)),
          ],
        ),
        body: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            removeBottom: false,
            child: Stack(
              children: [
                isOffline && UsersController.dictionary.isEmpty
                    ? Padding(
                        padding: const EdgeInsets.all(70),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 50),
                              child: Image.asset(
                                  "assets/dinosaurs/LandmarkNoConn.png",
                                  width: 500,
                                  fit: BoxFit.cover),
                            ),
                            Text(
                              "There is no connection to obtain your landmarks, so it may or may not contain places. Please try again later.",
                              style: Theme.of(context).textTheme.headline6,
                              textAlign: TextAlign.center,
                            )
                          ],
                        ))
                    : UsersController.dictionary.isNotEmpty
                        ? Center(
                            child: ListView.builder(
                                physics: const BouncingScrollPhysics(),
                                controller: scrollController,
                                itemCount:
                                    LandmarksController.listLandmarks.length +
                                        1,
                                itemBuilder: (BuildContext context, int index) {
                                  if (index == 0) {
                                    return popular == null
                                        ? Container()
                                        : StreakCard(
                                            color: app_colors.landmark,
                                            mainText: "Popular landmarks",
                                            subText: popular!,
                                            trailIcon: Icon(
                                              Icons.local_see_rounded,
                                              size: 50,
                                              color: app_colors.landmark,
                                            ));
                                  } else {
                                    final landmark = LandmarksController
                                        .listLandmarks[index - 1];
                                    return LangCard(
                                      color: app_colors.landmarkContainer,
                                      highlightColor:
                                          app_colors.landmarkContainer,
                                      title: landmark.originalName,
                                      subtitle: landmark.englishName,
                                      leadingIcon:
                                          showFlag(landmark.originalLanguage),
                                      trailIcon: ClickableIcon(
                                          iconOff: Icon(Icons.expand_more,
                                              color: app_colors.landmark),
                                          iconOn: Icon(Icons.expand_less,
                                              color: app_colors.landmark),
                                          type: TypeOfClickableButton.unique,
                                          onTap: () {
                                            resetLandmark(landmark);
                                          }),
                                      onTap: () {
                                        resetLandmark(landmark);
                                      },
                                      bottom: Column(children: <Widget>[
                                        landmark.photo != ""
                                            ? CachedNetworkImage(
                                                imageUrl: landmark.photo,
                                                errorWidget:
                                                    (context, url, error) =>
                                                        const Icon(Icons.error),
                                                width: 500,
                                                fit: BoxFit.cover,
                                              )
                                            : Image.asset("assets/no_image.jpg",
                                                width: 500, fit: BoxFit.cover),
                                        LandmarksController.mapDetail[
                                                landmark.englishName]!
                                            ? Padding(
                                                padding:
                                                    const EdgeInsets.all(16),
                                                child: Align(
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    child: Text(
                                                        landmark.description)))
                                            : Container(),
                                      ]),
                                    );
                                  }
                                }),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(70),
                            child: Center(
                              child: Column(children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 90),
                                  child: Image.asset(
                                      "assets/dinosaurs/LandmarkEmpty.png",
                                      width: 500,
                                      fit: BoxFit.cover),
                                ),
                                Text(
                                  "There are no visited landmarks.",
                                  style: Theme.of(context).textTheme.headline4,
                                  textAlign: TextAlign.center,
                                )
                              ]),
                            )),
                Container(
                  height: 25,
                ),
                if (isLoading)
                  Positioned(
                      bottom: 40,
                      left: size.width * 0.5 - 30,
                      child: const _LoadingIcon())
              ],
            )),
        floatingActionButton: FloatingActionButton(
            backgroundColor:
                isOffline ? app_colors.surface : app_colors.secondaryContainer,
            onPressed: isOffline
                ? () {
                    NotificationsService.showSnackbar(
                        "Check your internet connectivity to upload a new world to your dictionary.");
                  }
                : () async {
                    final picker = ImagePicker();
                    final XFile? pickedFile = await picker.pickImage(
                        source: ImageSource.camera, imageQuality: 100);
                    if (pickedFile != null) {
                      WordController.uploadImage(pickedFile)
                          .then((urlDownload) {
                        final landmarkCreationFuture =
                            LandmarksController.createLandmark(urlDownload)
                                .then((statusCode) {
                          if (statusCode == 200) {
                            setState(() {});
                          } else if (statusCode == 404) {
                            NotificationsService.showSnackbar(
                                "There was not detected any landmark.");
                          } else {
                            NotificationsService.showSnackbar(
                                "A problem occurs identifying the landmark on your photo. Check your connectivity and try again.");
                          }
                        });
                        final imageRecognitionFuture =
                            LanguagesController.getLocale(
                                    Preferences.nativeLang)
                                .then((nativeLocale) {
                          LanguagesController.getLocale(Preferences.currentLang)
                              .then((currentLocale) {
                            WordController.imageRecognition(
                                    urlDownload,
                                    nativeLocale,
                                    currentLocale,
                                    DateTime.now().millisecondsSinceEpoch,
                                    () {})
                                .catchError((error) {
                              NotificationsService.showSnackbar(
                                  "A problem occur when getting the objects in the image. Check your connectivity and try again.");
                            });
                          });
                        });
                        Future.wait([
                          landmarkCreationFuture,
                          imageRecognitionFuture
                        ]).then((value) {
                          Preferences.lastLandmark =
                              DateTime.now().millisecondsSinceEpoch.toString();
                          LandmarksController.resetLandmarks();
                          Navigator.pushNamed(context, 'landmark_add');
                        });
                      });
                    } else {
                      NotificationsService.showSnackbar(
                          "A problem occur uploading your photo. Check your connectivity and try again.");
                    }
                  },
            child: const Icon(Icons.camera_alt_sharp)),
        bottomNavigationBar: const Navbar());
  }
}

class _LoadingIcon extends StatelessWidget {
  const _LoadingIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        height: 60,
        width: 60,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.9), shape: BoxShape.circle),
        child: CircularProgressIndicator(color: app_colors.primary));
  }
}

showFlag(String s) {
  switch (s) {
    case "German":
      return const Image(
        image: AssetImage("assets/flags/1f1e9-1f1ea.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Spanish":
      return const Image(
        image: AssetImage("assets/flags/1f1ea-1f1f8.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Italian":
      return const Image(
        image: AssetImage("assets/flags/1f1ee-1f1f9.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "Portuguese":
      return const Image(
        image: AssetImage("assets/flags/1f1f5-1f1f9.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    case "English":
      return const Image(
        image: AssetImage("assets/flags/1f1fa-1f1f8.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
    default:
      return const Image(
        image: AssetImage("assets/flags/1f30e.png"),
        height: 36,
        fit: BoxFit.scaleDown,
      );
  }
}
