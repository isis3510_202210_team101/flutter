import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/providers/login_form_provider.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as colors;
import 'package:connectivity_plus/connectivity_plus.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 80),
            child: Column(
              children: [
                Column(
                  children: [
                    Text('Welcome to',
                        style: Theme.of(context).textTheme.headline6),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: Text('LangApp',
                            style: Theme.of(context).textTheme.headline2)),
                    Text('It\s nice to have you back',
                        style: Theme.of(context).textTheme.headline6),
                    ChangeNotifierProvider(
                        create: (_) => LoginFormProvider(), child: _LoginForm())
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}

class _LoginForm extends StatefulWidget {
  @override
  State<_LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<_LoginForm> {
  StreamSubscription? connection;
  bool isOffline = true;
  bool first = true;

  @override
  void initState() {
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          isOffline = true;
        });
      } else {
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 60),
        child: Form(
          key: loginForm.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: TextFormField(
                    autocorrect: false,
                    keyboardType: TextInputType.emailAddress,
                    style: const TextStyle(fontSize: 15),
                    onChanged: (value) => loginForm.email = value,
                    validator: (value) {
                      String pattern =
                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                      RegExp regExp = RegExp(pattern);

                      return regExp.hasMatch(value ?? '')
                          ? null
                          : 'The email is not valid.';
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        labelText: 'Email', border: OutlineInputBorder()),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: TextFormField(
                  autocorrect: false,
                  style: const TextStyle(fontSize: 15),
                  obscureText: true,
                  onChanged: (value) => loginForm.password = value,
                  validator: (value) {
                    return (value != null && value.length >= 6)
                        ? null
                        : 'Password must have at least 6 characters.';
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: const InputDecoration(
                      labelText: 'Password', border: OutlineInputBorder()),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 70),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(children: [
                        OutlinedButton.icon(
                          onPressed: () => Navigator.pushReplacementNamed(
                              context, 'welcome'),
                          icon: const Icon(Icons.arrow_back),
                          label: const Text(""),
                        )
                      ]),
                      const Spacer(),
                      Column(children: [
                        ElevatedButton(
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 65, vertical: 10),
                                child: const Text('Login')),
                            onPressed: loginForm.isLoading
                                ? null
                                : (isOffline
                                    ? () {
                                        NotificationsService.showSnackbar(
                                            "Check your internet connectivity to login.");
                                      }
                                    : () {
                                        FocusScope.of(context).unfocus();
                                        final authService =
                                            Provider.of<AuthService>(context,
                                                listen: false);

                                        loginForm.isLoading = true;

                                        authService
                                            .login(loginForm.email,
                                                loginForm.password)
                                            .then((errorMessage) {
                                          if (errorMessage == null) {
                                            Preferences.username =
                                                loginForm.email;
                                            UsersController.retrieveUser(
                                                Preferences.username,
                                                (s) => {});
                                            UsersController.setUserPreferences(
                                                    Preferences.username)
                                                .then((a) => Navigator
                                                    .pushReplacementNamed(
                                                        context, 'home'));
                                          } else {
                                            NotificationsService.showSnackbar(
                                                "Password or email invalid.");
                                            loginForm.isLoading = false;
                                          }
                                        });
                                      }),
                            style: isOffline? ElevatedButton.styleFrom(
                              primary: colors.surfaceVariant,
                              onPrimary: colors.outline,
                            ):ElevatedButton.styleFrom(
                              primary: colors.secondaryContainer,
                              onPrimary: Colors.black ))
                      ])
                    ],
                  )),
              Container(
                height: 25,
              )
            ],
          ),
        ));
  }
}
