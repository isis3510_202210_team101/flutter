export 'package:flutter_app_lang/views/check_auth.dart';
export 'package:flutter_app_lang/views/home.dart';
export 'package:flutter_app_lang/views/test.dart';
export 'package:flutter_app_lang/views/dictionary_add.dart';
export 'package:flutter_app_lang/views/dictionary_IR_feedback.dart';
export 'package:flutter_app_lang/views/welcome.dart';
export 'package:flutter_app_lang/views/login.dart';
export 'package:flutter_app_lang/views/register1.dart';
export 'package:flutter_app_lang/views/register2.dart';
export 'package:flutter_app_lang/views/onboarding_native_lang.dart';
export 'package:flutter_app_lang/views/onboarding_learning_lang.dart';
export 'package:flutter_app_lang/views/change_learning_language.dart';
export 'package:flutter_app_lang/views/landmarksList.dart';
export 'package:flutter_app_lang/views/landmark_add.dart';

