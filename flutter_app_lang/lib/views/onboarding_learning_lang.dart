import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'package:flutter_app_lang/components/langCard.dart';
import 'package:flutter_app_lang/services/services.dart';

class OnboardingLL extends StatefulWidget {
  const OnboardingLL({Key? key}) : super(key: key);

  @override
  State<OnboardingLL> createState() => _OnboardingLLState();
}

class _OnboardingLLState extends State<OnboardingLL> {
  List<Widget> content = [];
  List<String> learnLanguages = [];
  List flags = [];

  @override
  Widget build(BuildContext context) {
    List<Widget> content = [];
    dynamic element;
    for (int i = 0; i < LanguagesController.languages.length; i++) {
      element =  LanguagesController.languages[i];
      if (Preferences.nativeLang == element.englishLanguage) {
        continue;
      }
      AssetImage flag;
      try {
        flag = AssetImage("assets/flags/" + element.flag + ".png");
      } on Exception {
        flag = const AssetImage("assets/flags/1f30e.png");
      }

      content.add(LangCard(
        color: app_colors.surfaceVariant,
        highlightColor: app_colors.primaryContainer,
        title: element.language,
        subtitle: element.englishLanguage,
        trailIcon: Image(
          image: flag,
          height: 30,
          width: 36,
          fit: BoxFit.scaleDown,
        ),
        onTap: () {
          if (learnLanguages.contains(element.englishLanguage)) {
            learnLanguages.remove(element.englishLanguage);
            flags.remove(element.flag);
          } else {
            learnLanguages.add(element.englishLanguage);
            flags.add(element.flag);
          }
        },
      ));
    }

    content.add(Container(
      height: 25,
    ));

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'You want to learn...',
        ),
        actions: const [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.search_rounded),
          ),
        ],
      ),
      body: Center(child: ListView(children: content)),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (learnLanguages.isEmpty) {
            NotificationsService.showSnackbar(
                "Please select at least one language you want to learn");
          } else {
            UsersController.setLearn(learnLanguages);
            Preferences.flag = flags[0];
            Navigator.pushReplacementNamed(context, 'signup2');
          }
        },
        child: const Icon(Icons.check_rounded),
      ),
    );
  }
}
