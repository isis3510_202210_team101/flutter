import 'package:flutter/material.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/providers/signup_form_provider.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:provider/provider.dart';
import 'package:flutter_app_lang/styles/theme_colors.dart' as colors;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';

class RegisterView2 extends StatelessWidget {
  const RegisterView2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 90),
            child: Column(
              children: [
                Column(
                  children: [
                    Text('Welcome to',
                        style: Theme.of(context).textTheme.headline6),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: Text('LangApp',
                            style: Theme.of(context).textTheme.headline2)),
                    Text('Enter your credentials',
                        style: Theme.of(context).textTheme.headline6),
                    ChangeNotifierProvider(
                        create: (_) => SignUpProvider(),
                        child: const SignUpForm2())
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}

class SignUpForm2 extends StatefulWidget {
  const SignUpForm2({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SignUpForm2();
  }
}

class _SignUpForm2 extends State<SignUpForm2> {
  StreamSubscription? connection;
  bool isOffline = true;
  bool first = true;

  @override
  void initState() {
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          isOffline = true;
        });
      } else {
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  void displayError(String message) {
    NotificationsService.showSnackbar(message);
  }

  @override
  Widget build(BuildContext context) {
    final signUpForm = Provider.of<SignUpProvider>(context);
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 50),
        child: Form(
          key: signUpForm.formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 90),
                  child: TextFormField(
                    autocorrect: false,
                    keyboardType: TextInputType.emailAddress,
                    style: const TextStyle(fontSize: 15),
                    onChanged: (value) => signUpForm.email = value,
                    validator: (value) {
                      String pattern =
                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                      RegExp regExp = RegExp(pattern);

                      return regExp.hasMatch(value ?? '')
                          ? null
                          : 'The email is not valid.';
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                        labelText: 'Email', border: OutlineInputBorder()),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: TextFormField(
                  autocorrect: false,
                  style: const TextStyle(fontSize: 15),
                  obscureText: true,
                  onChanged: (value) => signUpForm.password = value,
                  validator: (value) {
                    return (value != null && value.length >= 6)
                        ? null
                        : 'Password must have at least 6 characters.';
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  decoration: const InputDecoration(
                      labelText: 'Password', border: OutlineInputBorder()),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 110),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(children: [
                        OutlinedButton.icon(
                          onPressed: () => Navigator.pushReplacementNamed(
                              context, 'onboardingLL'),
                          icon: const Icon(Icons.arrow_back),
                          label: const Text(""),
                        )
                      ]),
                      const Spacer(),
                      Column(children: [
                        ElevatedButton(
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 65, vertical: 10),
                                child: const Text(
                                  'Sign up',
                                )),
                            style: isOffline? ElevatedButton.styleFrom(
                              primary: colors.surfaceVariant,
                              onPrimary: colors.outline,
                            ):ElevatedButton.styleFrom(
                              onPrimary: Colors.white ),
                            onPressed: signUpForm.isLoading
                                ? null
                                : () {
                                    Connectivity()
                                        .checkConnectivity()
                                        .then((result) {
                                      if (result == ConnectivityResult.none) {
                                        NotificationsService.showSnackbar(
                                            "Check your internet connectivity to sign up.");
                                      } else {
                                        FocusScope.of(context).unfocus();
                                        final authService =
                                            Provider.of<AuthService>(context,
                                                listen: false);

                                        if (!signUpForm.isValidForm()) return;

                                        signUpForm.isLoading = true;

                                        authService
                                            .createUser(signUpForm.email,
                                                signUpForm.password)
                                            .then((errorMessage) {
                                          if (errorMessage == null) {
                                            UsersController.setEmail(
                                                signUpForm.email);
                                            Preferences.username =
                                                signUpForm.email;
                                            UsersController.postUser(
                                                displayError);
                                            Navigator.pop(context);
                                            Navigator.pushReplacementNamed(
                                                context, 'home');
                                          } else {
                                            NotificationsService.showSnackbar(
                                                "Email already exists.");
                                            signUpForm.isLoading = false;
                                          }
                                        });
                                      }
                                    });
                                  })
                      ])
                    ],
                  )),
              Container(
                height: 40,
              )
            ],
          ),
        ));
  }
}
