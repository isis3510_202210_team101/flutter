import 'package:flutter/material.dart';
import 'package:flutter_app_lang/components/clickableIcon.dart';
import 'package:flutter_app_lang/components/langCard.dart';
import 'package:flutter_app_lang/components/langCardImage.dart';
import 'package:flutter_app_lang/components/navbar.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'dart:async';

class DictionaryView extends StatefulWidget {
  const DictionaryView({Key? key}) : super(key: key);

  @override
  State<DictionaryView> createState() => _DictionaryView();
}

class _DictionaryView extends State<DictionaryView> {
  StreamSubscription? connection;
  bool first = true;
  bool isOffline = false;
  final ScrollController scrollController = ScrollController();
  bool isLoading = false;

  void setWordsDictionary() {
    if (mounted) {
      setState(() {});
    }
  }

  void resetWord(word) {
    UsersController.mapDetail[word.originalWord] =
        !UsersController.mapDetail[word.originalWord]!;
    setState(() {});
  }

  void setDetailWord(username, wordObj, mainLearningLang) {
    if (wordObj.description == "") {
      UsersController.getDetail(
          username, wordObj, mainLearningLang, setWordsDictionary);
    }
  }

  @override
  void initState() {
    UsersController.nextPageToken = "";
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          isOffline = true;
        });
      } else {
        //there is connection
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });

    scrollController.addListener(() {
      if (scrollController.position.pixels + 500 >=
          scrollController.position.maxScrollExtent) {
        if (UsersController.dictionary.isEmpty ||
            UsersController.nextPageToken != "") {
          fetchData();
        }
      }
    });

    String username = Preferences.username;
    String nativeLang = Preferences.nativeLang;
    String mainLearningLang = Preferences.currentLang;
    UsersController.retrieveDictionaryList(
        username, nativeLang, mainLearningLang, setWordsDictionary);
    super.initState();
  }

  Future fetchData() async {
    if (isLoading) return;

    isLoading = true;
    setState(() {});

    String username = Preferences.username;
    String nativeLang = Preferences.nativeLang;
    String mainLearningLang = Preferences.currentLang;
    UsersController.appendDictionaryList(
            username, nativeLang, mainLearningLang, setWordsDictionary)
        .then((value) async {
      await Future.delayed(const Duration(seconds: 4));
      isLoading = false;
      setState(() {});
      if (scrollController.position.pixels + 100 >=
          scrollController.position.maxScrollExtent) {
        return;
      }

      scrollController.animateTo(scrollController.position.pixels + 120,
          duration: const Duration(milliseconds: 300),
          curve: Curves.fastOutSlowIn);
    });
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (UsersController.mapDetail.isEmpty) {
      UsersController.resetDetail();
    }

    String username = Preferences.username;
    String mainLearningLang = Preferences.currentLang;
    final size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: const Text("My dictionary"),
          actions: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: isOffline
                    ? const Icon(Icons.cloud_off)
                    : const Icon(Icons.cloud_done)),
          ],
        ),
        body: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            removeBottom: false,
            child: Stack(
              children: [
                isOffline && UsersController.dictionary.isEmpty
                    ? Padding(
                        padding: const EdgeInsets.all(70),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 50),
                              child: Image.asset(
                                  "assets/dinosaurs/DictionaryNoConn.png",
                                  width: 500,
                                  fit: BoxFit.cover),
                            ),
                            Text(
                              "There is no connection to obtain your dictionary, so it may or may not contain words. Please try again later.",
                              style: Theme.of(context).textTheme.headline6,
                              textAlign: TextAlign.center,
                            )
                          ],
                        ))
                    : UsersController.dictionary.isNotEmpty
                        ? Center(
                            child: ListView.builder(
                                physics: const BouncingScrollPhysics(),
                                controller: scrollController,
                                itemCount: UsersController.dictionary.length,
                                itemBuilder: (BuildContext context, int index) {
                                  final word =
                                      UsersController.dictionary[index];
                                  return UsersController
                                          .mapDetail[word.originalWord]!
                                      ? LangCard(
                                          color: const Color(0xFFD9F6FF),
                                          highlightColor:
                                              const Color(0xFFD9F6FF),
                                          title: word.translation,
                                          subtitle: word.originalWord,
                                          trailIcon: ClickableIcon(
                                              iconOff: Icon(
                                                  Icons.bookmark_rounded,
                                                  color: app_colors.primary),
                                              iconOn: Icon(
                                                  Icons.bookmark_outline,
                                                  color: app_colors.primary),
                                              type:
                                                  TypeOfClickableButton.unique,
                                              onTap: () {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        title: const Text(
                                                            'Deleting'),
                                                        content: Text(
                                                            'Are you sure about deleting the word ' +
                                                                word.idWord +
                                                                ' from your dictionary?'),
                                                        actions: <Widget>[
                                                          TextButton(
                                                            child: const Text(
                                                                "Delete"),
                                                            onPressed: () {
                                                              WordController.deleteWordBackend(
                                                                      username,
                                                                      word
                                                                          .idWord)
                                                                  .then(
                                                                      (statusCode) {
                                                                showDialog(
                                                                  context:
                                                                      context,
                                                                  builder:
                                                                      (BuildContext
                                                                          context) {
                                                                    String
                                                                        title =
                                                                        'Error';
                                                                    String
                                                                        message =
                                                                        'An error ocurred when deleting the word from your dictionary.';
                                                                    if (statusCode ==
                                                                        204) {
                                                                      title =
                                                                          'Success';
                                                                      message = 'The word ' +
                                                                          word.idWord +
                                                                          ' was removed from your dictionary.';
                                                                    }
                                                                    return AlertDialog(
                                                                      title: Text(
                                                                          title),
                                                                      content: Text(
                                                                          message),
                                                                      actions: <
                                                                          Widget>[
                                                                        TextButton(
                                                                          child:
                                                                              const Text("OK"),
                                                                          onPressed:
                                                                              () {
                                                                            Navigator.of(context).pop();
                                                                            Navigator.of(context).pop();
                                                                            Navigator.pushReplacementNamed(context,
                                                                                "dictionary");
                                                                          },
                                                                        ),
                                                                      ],
                                                                    );
                                                                  },
                                                                );
                                                                UsersController
                                                                    .resetDictionary();
                                                                setWordsDictionary();
                                                              });
                                                            },
                                                          ),
                                                          TextButton(
                                                              child: const Text(
                                                                  "Cancel"),
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              })
                                                        ],
                                                      );
                                                    });
                                              }),
                                          onTap: (() => resetWord(word)),
                                          bottom: Column(children: <Widget>[
                                            word.photo != ""
                                                ? CachedNetworkImage(
                                                    imageUrl: word.photo,
                                                    errorWidget: (context, url,
                                                            error) =>
                                                        const Icon(Icons.error),
                                                    width: 500,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Image.asset(
                                                    "assets/no_image.jpg",
                                                    width: 500,
                                                    fit: BoxFit.cover),
                                            Padding(
                                              padding: const EdgeInsets.all(16),
                                              child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(word.description),
                                              ),
                                            ),
                                            ButtonBar(children: <Widget>[
                                              OutlinedButton(
                                                onPressed: () {},
                                                child: const Icon(
                                                    Icons.volume_up_rounded),
                                              ),
                                              ElevatedButton(
                                                onPressed: () {},
                                                child: const Text(
                                                    'Add to flashcards'),
                                              )
                                            ])
                                          ]))
                                      : LangCardImage(
                                          color: app_colors.surfaceVariant,
                                          highlightColor: isOffline
                                              ? app_colors.surfaceVariant
                                              : app_colors.primaryContainer,
                                          title: word.translation,
                                          subtitle: word.originalWord,
                                          image: word.thumbnail,
                                          onTap: (() {
                                            Connectivity()
                                                .checkConnectivity()
                                                .then((result) {
                                              if (result ==
                                                      ConnectivityResult.none &&
                                                  word.description == "") {
                                                NotificationsService.showSnackbar(
                                                    "Check your internet connectivity to open the word detail.");
                                              } else {
                                                resetWord(word);
                                                setDetailWord(username, word,
                                                    mainLearningLang);
                                              }
                                            });
                                          }));
                                }),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(70),
                            child: Center(
                              child: Column(children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 90),
                                  child: Image.asset(
                                      "assets/dinosaurs/DictionaryNoConn.png",
                                      width: 500,
                                      fit: BoxFit.cover),
                                ),
                                Text(
                                  "There is no words in your dictionary.",
                                  style: Theme.of(context).textTheme.headline4,
                                  textAlign: TextAlign.center,
                                )
                              ]),
                            )),
                Container(
                  height: 25,
                ),
                if (isLoading)
                  Positioned(
                      bottom: 40,
                      left: size.width * 0.5 - 30,
                      child: const _LoadingIcon())
              ],
            )),
        floatingActionButton: FloatingActionButton(
          backgroundColor:
              isOffline ? app_colors.surface : app_colors.secondaryContainer,
          onPressed: isOffline
              ? () {
                  NotificationsService.showSnackbar(
                      "Check your internet connectivity to upload a new world to your dictionary.");
                }
              : () {
                  WordController.createWordBody["photo"] = "";
                  Navigator.pushNamed(context, 'dictionary_add');
                },
          child: const Icon(Icons.add),
        ),
        bottomNavigationBar: const Navbar());
  }
}

class _LoadingIcon extends StatelessWidget {
  const _LoadingIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10),
        height: 60,
        width: 60,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.9), shape: BoxShape.circle),
        child: CircularProgressIndicator(color: app_colors.primary));
  }
}
