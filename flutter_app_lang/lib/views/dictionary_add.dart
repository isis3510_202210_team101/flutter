import 'package:flutter/material.dart';
import 'package:flutter_app_lang/shared_preferences/preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '../styles/theme_colors.dart' as app_colors;
import 'package:flutter_app_lang/components/navbar.dart';
import 'package:flutter_app_lang/controllers/controllers.dart';
import 'package:flutter_app_lang/providers/word_create_provider.dart';
import 'package:flutter_app_lang/services/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';

class DictionaryAddView extends StatefulWidget {
  const DictionaryAddView({Key? key}) : super(key: key);
  @override
  State<DictionaryAddView> createState() => _DictionaryAddState();
}

class _DictionaryAddState extends State<DictionaryAddView> {
  StreamSubscription? connection;
  bool isOffline = true;
  bool first = true;

  @override
  void initState() {
    final Connectivity _connectivity = Connectivity();
    _connectivity.checkConnectivity().then((connectivityResult) => {
          if (connectivityResult == ConnectivityResult.none)
            {isOffline = true}
          else
            {isOffline = false}
        });
    connection =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          isOffline = true;
        });
      } else {
        setState(() {
          isOffline = false;
          first = true;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    connection!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    WordCreateProvider? wordCreationForm;
    TextEditingController txt = TextEditingController();
    txt.text = " ";
    Image imageToShow = WordController.createWordBody["photo"] == ""
        ? const Image(image: AssetImage("assets/take-photo.png"))
        : Image.network(WordController.createWordBody["photo"]);

    void setProvider(wordCreationFormProvider) {
      wordCreationForm = wordCreationFormProvider;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Add new word"),
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        InkWell(
          //child: SvgPicture.asset("assets/take-photo.svg",)
          child: imageToShow,
          onTap: () {
            Connectivity().checkConnectivity().then((result) async {
              if (result == ConnectivityResult.none) {
                NotificationsService.showSnackbar(
                    "Check your internet connectivity to use the image recognition.");
              } else {
                txt.text = " ";
                if (!wordCreationForm!.isLoading) {
                  if (!wordCreationForm!.isValidForm()) return;
                  final picker = ImagePicker();
                  final XFile? pickedFile = await picker.pickImage(
                      source: ImageSource.camera, imageQuality: 100);
                  if (pickedFile != null) {
                    WordController.uploadImage(pickedFile).then((urlDownload) {
                      WordController.createWordBody["photo"] = urlDownload;
                      Navigator.pushNamed(context, 'dictionary_IR_feedback');
                    }).catchError((error) {
                      NotificationsService.showSnackbar(
                          "A problem occur uploading your photo. Check your connectivity and try again.");
                    });
                  }
                }
              }
            });
          },
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            child: ChangeNotifierProvider(
                create: (_) => WordCreateProvider(),
                child: WordCreateForm(
                    setProvider: setProvider, txtController: txt)))
      ])),
      bottomNavigationBar: const Navbar(),
      floatingActionButton: FloatingActionButton(
        backgroundColor:
              isOffline ? app_colors.surface : app_colors.secondaryContainer,
        child: const Icon(Icons.check_rounded),
        onPressed: () {
          isOffline
              ? () {
                  NotificationsService.showSnackbar(
                      "Check your internet connectivity to upload a new word to your dictionary.");
                }
              : () {
                  if (WordController.createWordBody["word"] == " " ||
                      txt.text == "") {
                    WordController.createWordBody["word"] == "";
                    txt.text = "";
                  }
                  if (!wordCreationForm!.isValidForm()) return;
                  wordCreationForm!.isLoading = true;
                  WordController.createWord().then((statusCode) {
                    if (statusCode == 200) {
                      UsersController.resetDictionary();
                      wordCreationForm!.isLoading = false;
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('Success'),
                              content: Text('The word ' +
                                  WordController.createWordBody['word'] +
                                  ' was added successfully to your dictionary.'),
                              actions: <Widget>[
                                TextButton(
                                    child: const Text("OK"),
                                    onPressed: () {
                                      WordController.resetCreateWordBody();
                                      Navigator.pushReplacementNamed(
                                          context, 'dictionary');
                                    })
                              ],
                            );
                          });
                    } else if (statusCode == 400) {
                      NotificationsService.showSnackbar(
                          "The word you want to add is already in your dictionary.");
                    } else if (statusCode == 404) {
                      NotificationsService.showSnackbar(
                          "You must provide a valid word in the selected language.");
                    } else {
                      NotificationsService.showSnackbar(
                          "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                    }
                  }).catchError((error) {
                    NotificationsService.showSnackbar(
                        "An error occur inserting the word in your dictionary. Check your connectivity and try again.");
                  });
                };
        },
      ),
    );
  }
}

class WordCreateForm extends StatefulWidget {
  final Function setProvider;
  final TextEditingController txtController;

  const WordCreateForm(
      {Key? key, required this.setProvider, required this.txtController})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _WordCreateForm();
  }
}

class _WordCreateForm extends State<WordCreateForm> {
  List<String> learnedLanguages = [];

  void setLearnedLanguages(List<String> receivedLearnedLanguages) {
    learnedLanguages = receivedLearnedLanguages;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final wordCreationForm = Provider.of<WordCreateProvider>(context);
    widget.setProvider(wordCreationForm);
    UsersController.retrieveUser(Preferences.username, setLearnedLanguages);
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 60),
        child: Form(
          key: wordCreationForm.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                style: const TextStyle(fontSize: 15),
                controller: widget.txtController,
                onTap: () {
                  if (widget.txtController.text == " ") {
                    widget.txtController.text = "";
                  }
                },
                onChanged: (value) {
                  wordCreationForm.word = value.toString();
                  WordController.createWordBody["word"] = value.toString();
                },
                validator: (value) {
                  if (value == "" || value == null) {
                    return 'This field is required.';
                  }
                },
                initialValue: null,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: const InputDecoration(
                    labelText: 'Word to add', border: OutlineInputBorder()),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
              ),
              DropdownButtonFormField(
                style: const TextStyle(fontSize: 15, color: Colors.black),
                items: learnedLanguages
                    .map((lang) => DropdownMenuItem(
                          value: lang,
                          child: Text(lang),
                        ))
                    .toList(),
                validator: (value) {
                  if (value == "" || value == null) {
                    return 'This field is required.';
                  }
                },
                elevation: 1,
                onChanged: (value) {
                  wordCreationForm.language = value.toString();
                  WordController.createWordBody["learningLanguage"] =
                      value.toString();
                },
                decoration: const InputDecoration(
                  labelStyle: TextStyle(fontSize: 15),
                  labelText: 'Language',
                  border: OutlineInputBorder(),
                ),
              )
            ],
          ),
        ));
  }
}
