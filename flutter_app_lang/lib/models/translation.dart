class Translation {
  
  String description;
  final String englishWord;
  final String language;
  final String learningScore;
  final String translation;

  Translation(
      {required this.description,
      required this.englishWord,
      required this.language,
      required this.learningScore,
      required this.translation});

  factory Translation.fromJson(Map<String, dynamic> json) {
    return Translation(
        description: "",
        englishWord: json["fields"]["englishWord"]["referenceValue"].split("/").last.toString(),
        language: json["fields"]["language"]["referenceValue"].split("/").last.toString(),
        learningScore: json["fields"]["learningScore"]["integerValue"],
        translation: json["fields"]["translation"]["stringValue"]);
  }

  @override
  String toString() {
    return language + "-" + translation;
  }
}