class Word {
  final String idWord;
  final String thumbnail;
  final String originalLanguage;
  
  String photo;
  String originalWord;
  String translation;
  String description;

  Word(
      {required this.idWord,
      required this.thumbnail,
      required this.photo,
      required this.originalLanguage,
      required this.originalWord,
      required this.translation,
      required this.description});

  factory Word.fromJson(Map<String, dynamic> json) {
    return Word(
        idWord: json["name"].split("-").last.toString(),
        thumbnail: json["fields"]["thumbnail"]["stringValue"],
        originalLanguage: json["fields"]["originalLanguage"]["referenceValue"].split("/").last.toString(),
        photo: '',
        originalWord: '',
        translation: '',
        description: '');
  }

  @override
  String toString() {
    return idWord + " Original word: " + originalWord + " | " + "Translation: " + translation + " | Description: " + description;
  }
}
