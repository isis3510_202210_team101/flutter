class User {
  final String name;
  final String lastname;
  String email;
  String nativeLanguage;
  final String residenceCountry;
  final String birthDate;
  List<String> learnedLanguages;
  bool duolingo;

  User({
    required this.name,
    required this.lastname,
    required this.email,
    required this.nativeLanguage,
    required this.residenceCountry,
    required this.birthDate,
    required this.learnedLanguages,
    required this.duolingo,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        name: json["fields"]["name"]["stringValue"],
        duolingo: false,
        lastname: json["fields"]["lastname"]["stringValue"],
        email: json["fields"]["email"]["stringValue"],
        nativeLanguage: json["fields"]["nativeLanguage"]["referenceValue"]
            .split("/")
            .last
            .toString(),
        residenceCountry: json["fields"]["residenceCountry"]["stringValue"],
        birthDate: json["fields"]["birthDate"]["timestampValue"],
        learnedLanguages: List<String>.from(json["fields"]["learnedLanguages"]
                ["arrayValue"]["values"]
            .map((jsonObj) =>
                jsonObj["referenceValue"].split("/").last.toString())
            .toList()),);
  }

  Map toJson() => {
    'name': name,
    'lastname': lastname,
    'email': email,
    'birthDate': birthDate,
    'nativeLanguage': nativeLanguage,
    'residenceCountry': residenceCountry,
    'learnedLanguages': learnedLanguages,
    'usedDuolingo': duolingo
  };
}
