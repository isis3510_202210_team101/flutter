class Landmark {
  String englishName;
  String originalLanguage;
  String originalName;
  int visits;
  String description;
  String date;
  String photo;

  Landmark(
      {
      required this.englishName,
      required this.originalLanguage,
      required this.originalName,
      required this.visits,
      required this.date,
      required this.description,
      required this.photo,});

  factory Landmark.fromJson(Map<String, dynamic> json) {
    return Landmark(
        englishName: json["name"].split("-").last.toString(),
        originalLanguage: '',
        originalName: '',
        visits: 0,
        description: '',
        photo: json["fields"]["photo"]["stringValue"],
        date: json["fields"]["date"]["timestampValue"],
        );
  }
}
