class NearWord {
  final String word;
  final String latitude;
  final String longitude;

  NearWord({
    required this.word,
    required this.latitude,
    required this.longitude,
  });

  factory NearWord.fromJson(Map<String, dynamic> json) {
    return NearWord(
        word: json["word"],
        latitude: json["location"]["_latitude"],
        longitude: json["location"]["_longitude"]);
  }

}
