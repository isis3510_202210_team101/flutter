class Language {
  final String flag;
  final String language;
  final String locale;
  final String englishLanguage;

  Language(
      {required this.flag,
      required this.language,
      required this.locale,
      required this.englishLanguage});

  factory Language.fromJson(Map<String, dynamic> json) {
    return Language(
        flag: json["fields"]["flag"]["stringValue"],
        language: json["fields"]["language"]["stringValue"],
        locale: json["fields"]["locale"]["stringValue"],
        englishLanguage: json["name"].split("/").last.toString());
  }
}
