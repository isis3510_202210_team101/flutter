import 'package:flutter/material.dart';

Color primary = const Color(0xFF00677D);
Color onPrimary = const Color(0xFFFFFFFF);
Color primaryContainer = const Color(0xFFAFECFF);
Color onPrimaryContainer = const Color(0xFF001F28);

Color secondary = const Color(0xFF785900);
Color onSecondary = const Color(0xFFFFFFFF);
Color secondaryContainer = const Color(0xFFFFDF94);
Color onSecondaryContainer = const Color(0xFF261A00);

Color tertiary = const Color(0xFF2F5DA8);
Color onTertiary = const Color(0xFFFFFFFF);
Color tertiaryContainer = const Color(0xFFD6E2FF);
Color onTertiaryContainer = const Color(0xFF001A42);

Color error = const Color(0xFFB3261E);
Color errorContainer = const Color(0xFFF9DEDC);
Color onError = const Color(0xFFFFFFFF);
Color onErrorContainer = const Color(0xFF410E0B);

Color background = const Color(0xFFF6FDFF);
Color onBackground = const Color(0xFF1C1B1E);

Color surface = const Color(0xFFFFFBFE);
Color onSurface = const Color(0xFF1C1B1E);
Color surfaceVariant = const Color(0xFFE7E0EC);
Color onSurfaceVariant = const Color(0xFF49454F);
Color outline = const Color(0xFF79747E);
Color onInverseSurface = const Color(0xFFF4EFF3);
Color inverseSurface = const Color(0xFF313033);
Color inversePrimary = const Color(0xFF58D5F7);
Color shadow = const Color(0xFF000000);

Color landmark = const Color(0xFF5651b6);
Color onLandmark = const Color(0xFFFFFFFF);
Color landmarkContainer = const Color(0xFFE4DFFF);
Color onLandmarkContainer = const Color(0xFF0F006A);