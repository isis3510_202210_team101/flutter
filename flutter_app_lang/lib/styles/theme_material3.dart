/*
import 'package:flutter/material.dart';

const seed = Color(0xFFd9eff7);
const landmark = Color(0xFF5651b6);
const onLandmark = Color(0xFFFFFFFF);
const landmarkContainer = Color(0xFFE4DFFF);
const onLandmarkContainer = Color(0xFF0F006A);

const lightColorScheme = ColorScheme(
	brightness: Brightness.light,
	primary : Color(0xFF00677D),
	onPrimary : Color(0xFFFFFFFF),
	primaryContainer : Color(0xFFAFECFF),
	onPrimaryContainer : Color(0xFF001F28),
	secondary : Color(0xFF785900),
	onSecondary : Color(0xFFFFFFFF),
	secondaryContainer : Color(0xFFFFDF94),
	onSecondaryContainer : Color(0xFF261A00),
	tertiary : Color(0xFF2F5DA8),
	onTertiary : Color(0xFFFFFFFF),
	tertiaryContainer : Color(0xFFD6E2FF),
	onTertiaryContainer : Color(0xFF001A42),
	error : Color(0xFFB3261E),
	errorContainer : Color(0xFFF9DEDC),
	onError : Color(0xFFFFFFFF),
	onErrorContainer : Color(0xFF410E0B),
	background : Color(0xFFFFFBFE),
	onBackground : Color(0xFF1C1B1E),
	surface : Color(0xFFFFFBFE),
	onSurface : Color(0xFF1C1B1E),
	surfaceVariant : Color(0xFFE7E0EC),
	onSurfaceVariant : Color(0xFF49454F),
	outline : Color(0xFF79747E),
	onInverseSurface : Color(0xFFF4EFF3),
	inverseSurface : Color(0xFF313033),
	inversePrimary : Color(0xFF58D5F7),
	shadow : Color(0xFF000000),
);

const darkColorScheme = ColorScheme(
	brightness: Brightness.dark,
	primary : Color(0xFF58D5F7),
	onPrimary : Color(0xFF003542),
	primaryContainer : Color(0xFF004E5F),
	onPrimaryContainer : Color(0xFFAFECFF),
	secondary : Color(0xFFF0C048),
	onSecondary : Color(0xFF3F2E00),
	secondaryContainer : Color(0xFF5B4300),
	onSecondaryContainer : Color(0xFFFFDF94),
	tertiary : Color(0xFFABC7FF),
	onTertiary : Color(0xFF002E69),
	tertiaryContainer : Color(0xFF0A458E),
	onTertiaryContainer : Color(0xFFD6E2FF),
	error : Color(0xFFF2B8B5),
	errorContainer : Color(0xFF8C1D18),
	onError : Color(0xFF601410),
	onErrorContainer : Color(0xFFF9DEDC),
	background : Color(0xFF1C1B1E),
	onBackground : Color(0xFFE6E1E6),
	surface : Color(0xFF1C1B1E),
	onSurface : Color(0xFFE6E1E6),
	surfaceVariant : Color(0xFF49454F),
	onSurfaceVariant : Color(0xFFCAC4D0),
	outline : Color(0xFF938F99),
	onInverseSurface : Color(0xFF1C1B1E),
	inverseSurface : Color(0xFFE6E1E6),
	inversePrimary : Color(0xFF00677D),
	shadow : Color(0xFF000000),
);*/
