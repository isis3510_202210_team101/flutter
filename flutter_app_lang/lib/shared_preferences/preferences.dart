import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static late SharedPreferences _prefs;

  static String _username = '';

  static String _nativeLang = '';

  static String _currentLang = '';

  static String _flag = '';

  static String _lastAccess = "";

  static int _streak = 0;

  static String _lastLandmark = '';

  static Future init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static String get username {
    return _prefs.getString('username') ?? _username;
  }

  static String get nativeLang {
    return _prefs.getString('nativeLang') ?? _nativeLang;
  }

  static String get currentLang {
    return _prefs.getString('currentLang') ?? _currentLang;
  }

  static String get flag {
    return _prefs.getString('flag') ?? _flag;
  }

  static String get lastAccess {
    return _prefs.getString('lastAccess') ?? _lastAccess;
  }

  static int get streak {
    return _prefs.getInt('streak') ?? _streak;
  }

  static String get lastLandmark {
    return _prefs.getString('lastLandmark') ?? _lastLandmark;
  }

  static set flag(String flag) {
    _flag = flag;
    _prefs.setString('flag', flag);
  }

  static set username(String username) {
    _username = username;
    _prefs.setString('username', username);
  }

  static set nativeLang(String nativeLang) {
    _nativeLang = nativeLang;
    _prefs.setString('nativeLang', nativeLang);
  }

  static set currentLang(String currentLang) {
    _currentLang = currentLang;
    _prefs.setString('currentLang', currentLang);
  }

  static set lastAccess(String lastAccess) {
    _lastAccess = lastAccess;
    _prefs.setString('lastAccess', lastAccess);
  }

  static set streak(int streak) {
    _streak = streak;
    _prefs.setInt('streak', streak);
  }

  static set lastLandmark(String lastLandmark) {
    _lastLandmark = lastLandmark;
    _prefs.setString('lastLandmark', currentLang);
  }
}
